(ns ^{:author "Sławek Gwizdowski"
      :doc "Development scratchpad."}
 dev
 (:require
   [clojure.core :refer :all]
   [clojure.repl :refer :all]
   [clojure.pprint :refer [pprint print-table]]
   [clojure.walk :refer [walk postwalk prewalk]]
   [clojure.spec.alpha :as s]
   [eftest.runner :refer [find-tests run-tests]]
   [szew.io :as io]
   [szew.io.util :as util]
   [szew.essbase.aso :as ess.aso]
   [szew.essbase.bso :as ess.bso]
   [szew.essbase.cols :as ess.cols]
   [szew.essbase.logs :as ess.logs]
   [szew.essbase.txl :as ess.txl]
   [szew.essbase.otl :as ess.otl]
   [szew.essbase.otl.member :as mbr]
   [szew.essbase.otl.dimension :as dim]))

(defn retest []
  (require '[szew.essbase.aso-test] :reload-all)
  (require '[szew.essbase.bso-test] :reload-all)
  (require '[szew.essbase.cols-test] :reload-all)
  (require '[szew.essbase.logs-test] :reload-all)
  (require '[szew.essbase.txl-test] :reload-all)
  (require '[szew.essbase.otl-test] :reload-all)
  (run-tests (find-tests "test")))

;; Process the outlines (XML exports)

(defn outlines!
 ([fname]
  (def otl fname)

  ;; XML -> dimension roots, sans members
  (def dimension-list (ess.otl/list-dimensions! otl))

  ;; XML -> full dimensions
  (def dimensions (ess.otl/extract-dimensions! otl))

  ;; XML -> cube data structure
  (def cube (ess.otl/cube! otl))

  ;; XML to member -> dimension lookup table (hash-map)
  (def lut (ess.otl/member-lut! otl))

  ;; XML to {:m->d {...} :dimensions [...] :dim-count N}
  (def lut-pkg (ess.otl/member-lut-package! otl))
  :done)
 ([]
  (outlines! "datasets/sample_basic.xml")))

;; library expecation check for dimensions
(defn borked-dims []
  (letfn [(de-member [m] (assoc m :members []))
          (pick? [m] (not (s/valid? ::dim/dimension m)))]
    (filter pick? (map de-member dimensions))))

;; library expectation check for member
(defn borked-members []
  (letfn [(de-member [m] (assoc m :members []))
          (pick? [m] (not (s/valid? ::mbr/member m)))]
    (->> dimensions
      (mapcat (comp (partial drop 1) ess.otl/member-seq))
      (map de-member)
      (filter pick?))))

;; Process native BSO exports

;; Requires member->dimension lookup and number of dimensions, we need lut-pkg
(defn block-storage! []
  (when-not (bound? #'lut-pkg)
    (outlines!))
  (let [bso  "datasets/sample_basic_level0.txt"
        tnc  (fn [[coords value]]
              (and (= "Texas" (coords "Market"))
                   (= "COGS" (coords "Measures"))))
        ;; attach :processor to lut-pkg and you're ready to go!
        pick (ess.bso/cells (assoc lut-pkg :processor (partial filterv tnc)))]
    (def texas-cogs (pick bso))
    :done))

;; Transaction Logs processing aka TxL
;; Filtering, matching, drift discovery and correction.

(defn trails! []
  (let [p (comp ess.txl/printable
                (partial filterv (ess.txl/user-matches? #"johny")))
        alg "datasets/sample.alg"
        atx "datasets/sample.atx"
        btx "datasets/sample_borked.atx"]

    (def txl    (io/in! (ess.txl/tx-log {:processor p}) {:alg alg :atx atx}))
    (def txl1   ((ess.txl/tx-log {:processor first}) {:alg alg :atx atx}))
    (def txl2   ((ess.txl/tx-log {:processor second}) {:alg alg :atx atx}))
    (def txlc   ((ess.txl/tx-log {:processor count}) {:alg alg :atx atx}))
    (def drifts ((ess.txl/tx-log {:processor ess.txl/drifts})
                 {:alg alg :atx btx}))
    (def alg-c  ((ess.txl/tx-log {:processor ess.txl/correct-drifts})
                 {:alg alg :atx atx}))
    (def alg-f  ((ess.txl/tx-log {:processor ess.txl/correct-drifts})
                 {:alg alg :atx btx}))
    :done))

;; MaxL spool: consume MaxL output, need to just know (set) column width right.

(defn spools! []
  (def spools (io/in! (ess.logs/maxl-spool {:processor vec, :column-width 256})
                      "datasets/maxl-output.log"))
  :done)
