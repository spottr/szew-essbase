(ns user
  (:require
    [clojure.core :refer :all]))

(defn dev []
  (require '[dev :refer :all])
  (in-ns 'dev)
  :loaded)
