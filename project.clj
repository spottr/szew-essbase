(defproject szew/essbase "0.3.5-SNAPSHOT"

  :description "Consume various files produced by Essbase."
  :url "https://bitbucket.org/spottr/szew-essbase"

  :license {:name "MIT Public License"
            :distribution :repo
            :comments "LICENSE file in project root directory."}

  :dependencies [[org.clojure/clojure "1.12.0"]
                 ;; Basic szew/io library.
                 [szew/io "0.5.7" :exclusions [orc.clojure/clojure]]
                 ;; Proper CSV handling, combat proven.
                 [clojure-csv/clojure-csv "2.0.2"]
                 ;; Date and Time manipulation.
                 [clojure.java-time "1.4.3"]
                 ;; Proper names and stuff.
                 [camel-snake-kebab "0.4.3"]]

  :profiles {:dev    {:dependencies [[criterium "0.4.6"]
                                     [orchestra "2021.01.01-1"]
                                     [eftest "0.6.0"]]
                      :plugins [[lein-codox "0.10.8"]]
                      :source-paths ["dev/src"]
                      :eastwood {:exclude-linters [:def-in-def :deprecations]
                                 :debug [:time]}
                      ;; codox
                      :jvm-opts []}
             :test    {:jvm-opts []}
             :uberjar {:aot :all
                       :jvm-opts []}}

  :global-vars {*warn-on-reflection* true
                *assert* false}

  :aot :all

  :codox {;:metadata {:doc/format :markdown}
          :project {:name "szew/essbase"}
          :namespaces [#"^szew\.essbase\.\w+$"]
          ;; that was fun:
          :source-uri
          ~(str "https://bitbucket.org/spottr/szew-essbase/src/"
                "320804db04e8a33311ac62d3de9dfc46b33c1d4c" ; 0.3.4
                "/{filepath}#{basename}-{line}")})
