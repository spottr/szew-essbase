(ns ^{:author "Sławek 'smg' Gwizdowski"
      :doc "Testing helpers."}
 szew.helpers-test)

(defn file-please!
  "Gimme a temporary File.
  "
  []
  (io! "Mr Java.Io.File, bring me a File!"
    (doto (java.io.File/createTempFile "szew-io-test-" ".bin")
      (.deleteOnExit))))

(defn thanks-for-the-file!
  "Delete a File.
  "
  [^java.io.File f]
  (when f
    (.delete f)))

