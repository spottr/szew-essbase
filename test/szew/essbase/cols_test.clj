; Copyright (c) Sławek Gwizdowski
;
; Permission is hereby granted, free of charge, to any person obtaining
; a copy of this software and associated documentation files (the "Software"),
; to deal in the Software without restriction, including without limitation
; the rights to use, copy, modify, merge, publish, distribute, sublicense,
; and/or sell copies of the Software, and to permit persons to whom the
; Software is furnished to do so, subject to the following conditions:
;
; The above copyright notice and this permission notice shall be included
; in all copies or substantial portions of the Software.
;
; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
; OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
; FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
; THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
; LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
; FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
; IN THE SOFTWARE.
;
(ns ^{:author "Sławek Gwizdowski"
      :doc "Essbase columns export - tests."}
 szew.essbase.cols-test
 (:require
   [szew.essbase.cols
    :refer [members records cells sniff-unknown sniff-dimensions dump->tsv]]
   [szew.essbase.bso-test :refer [m->d]]
   [szew.io :as io]
   [clojure.test :refer [deftest testing is]]
   [orchestra.spec.test :as st]
   [szew.helpers-test
    :refer [file-please! thanks-for-the-file!]]))

(st/instrument)

;; ## State

(def state (atom nil))

;; ## Tests

(deftest Members-processing-testing

  (let [sauce "datasets/sample_basic_level0_columns.txt"]
    (testing "Members"
      (let [mset (io/in! (members {:dim-count 5}) sauce)
            mset2 ((members {:dim-count 5}) sauce)
            mset3 (members {:dim-count 5} sauce)]
        (is (thrown? Exception (io/in! (members) sauce)))
        (is (= 28 (count mset)))
        (is (true? (contains? mset "Sales")))
        (is (true? (contains? mset "New York")))
        (is (true? (contains? mset "300-10")))
        (is (= mset mset2))
        (is (= mset mset3))))

    (testing "Members - unknown"
      (let [e-au (comp (partial into (hash-set)) (sniff-unknown {}))
            uset (io/in! (members {:dim-count 5 :processor e-au})
                         sauce)]
        (is (= 28 (count uset)))
        (is (true? (contains? uset "Sales")))
        (is (true? (contains? uset "New York")))
        (is (true? (contains? uset "300-10")))))))

(deftest Records-processing-testing

  (let [sauce "datasets/sample_basic_level0_columns.txt"]
    (testing "Records"
      (is (thrown? Exception (io/in! (records) sauce)))
      (is (thrown? Exception (io/in! (records {:dim-count 5}) sauce)))

      (let [sample  (io/in! (records {:dim-count 5 :m->d m->d :processor vec})
                            sauce)
            sample2 (io/in! (records {:dim-count 5 :m->d m->d :processor vec})
                            sauce)
            sample3 (io/in! (records {:dim-count 5 :m->d m->d :processor vec})
                            sauce)]
        (is (= [{:location "New York"
                 :measure  "100-10"
                 :period   "Jan"
                 :scenario "Actual"
                 }
                {"Sales"     "10"
                 "COGS"      "100"
                 "Marketing" "1000"
                 "Payroll"   "10000"
                 "Misc"      "100000"}]
               (first sample)))
        (is (= [{:location  "Ohio"
                 :measure   "300-10"
                 :period    "Jan"
                 :scenario  "Actual"
                 }
                {"Sales"     "1"
                 "COGS"      "2"
                 "Marketing" "3"
                 "Payroll"   "4"
                 "Misc"      "5"}]
               (last sample)))
        (is (= sample sample2))
        (is (= sample sample3))))

    (testing "Records - error reporting"
      (is (thrown? Exception (io/in! (records) sauce)))
      (is (thrown? Exception (io/in! (records {:dim-count 5}) sauce)))

      (let [ex (try (io/in! (records {:dim-count 5}) sauce)
                    (catch Exception ex ex))]
        (is (= 1 (-> ex ex-data :data-line)))
        (is (= sauce (-> ex ex-data :record meta :source)))))))

(deftest Cells-processing-testing

  (let [sauce "datasets/sample_basic_level0_columns.txt"]
    (testing "Cells"
      (is (thrown? Exception (io/in! (cells) sauce)))
      (is (thrown? Exception (io/in! (cells {:dim-count 5}) sauce)))

      (let [sample  (io/in! (cells {:dim-count 5 :m->d m->d :processor vec})
                           sauce)
            sample2 ((cells {:dim-count 5 :m->d m->d :processor vec}) sauce)
            sample3 (cells {:dim-count 5 :m->d m->d :processor vec} sauce)]
        (is (= [{:account  "Sales"
                 :location "New York"
                 :measure  "100-10"
                 :period   "Jan"
                 :scenario "Actual"
                 }
                "10"]
               (first sample)))
        (is (= [{:account  "COGS"
                 :location "New York"
                 :measure  "100-10"
                 :period   "Jan"
                 :scenario "Actual"
                 }
                "100"]
               (second sample)))
        (is (= [{:account   "Payroll"
                 :location  "Ohio"
                 :measure   "300-10"
                 :period    "Jan"
                 :scenario  "Actual"
                 }
                "4"]
               (last (butlast sample))))
        (is (= [{:account   "Misc"
                 :location  "Ohio"
                 :measure   "300-10"
                 :period    "Jan"
                 :scenario  "Actual"
                 }
                "5"]
               (last sample)))
        (is (= sample sample2))
        (is (= sample sample3))))

    (testing "Cells - error reporting"
      (is (thrown? Exception (io/in! (cells) sauce)))
      (is (thrown? Exception (io/in! (cells {:dim-count 5}) sauce)))

      (let [ex (try (io/in! (cells {:dim-count 5}) sauce)
                    (catch Exception ex ex))]
        (is (= 1 (-> ex ex-data :data-line)))
        (is (= sauce
               (-> ex ex-data :record meta :source)))))

    (testing "Cells - dimension sniffing"
      (let [cs (cells {:dim-count 5 :processor sniff-dimensions :m->d m->d})]
        (is (= #{:account :period :measure :location :scenario}
               (set (cs sauce))))))))

(deftest Columns-dump-to-tsv-testing
  (let [tmp    (file-please!)
        _      (dump->tsv m->d
                          [:account :location :measure :scenario :period]
                          tmp
                          "datasets/sample_basic_level0_columns.txt")
        master (io/in! (io/tsv) "datasets/sample_basic_level0_sanitized.tsv")
        newbie (io/in! (io/tsv) tmp)]
    (is (= master newbie))
    (is (thrown? Exception
                 (dump->tsv m->d
                            [:account :location :measure
                             :scenario :year :period]
                            tmp
                            "datasets/sample_basic_level0_unknown.txt")))
    (thanks-for-the-file! tmp)))
