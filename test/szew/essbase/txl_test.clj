; Copyright (c) Sławek Gwizdowski
;
; Permission is hereby granted, free of charge, to any person obtaining
; a copy of this software and associated documentation files (the "Software"),
; to deal in the Software without restriction, including without limitation
; the rights to use, copy, modify, merge, publish, distribute, sublicense,
; and/or sell copies of the Software, and to permit persons to whom the
; Software is furnished to do so, subject to the following conditions:
;
; The above copyright notice and this permission notice shall be included
; in all copies or substantial portions of the Software.
;
; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
; OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
; FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
; THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
; LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
; FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
; IN THE SOFTWARE.
;
(ns ^{:author "Sławek Gwizdowski"
      :doc "Essbase transaction logs - tests."}
 szew.essbase.txl-test
 (:require
   [szew.essbase.txl :as ess.txl
    :refer [headers blocks correct-alg-drifts!
            between? user-matches? data-matches? printable]]
   [szew.io :as io]
   [orchestra.spec.test :as st]
   [clojure.test :refer [deftest testing is]]
   [szew.helpers-test :refer [file-please! thanks-for-the-file!]])
 (:import [java.io File]))

(st/instrument)

(deftest ALG-to-headers-test

  (testing "ALG headers"

    (let [alg "datasets/sample.alg"
          heads (io/in! (io/lines {:eol "\r\n" :processor (comp vec headers)})
                        alg)]
      (is (= 9 (count heads)))
      (is (= 1 (:at (first heads))))
      (is (= 4 (:rows (first heads))))
      (is (= "2011-01-20T06:52:24" (:date (first heads))))
      (is (= 43 (:at (last heads))))
      (is (= 6 (:rows (last heads))))
      (is (= "2011-01-29T03:56:53" (:date (last heads))))
      (is (= 1 (:index (first heads))))
      (is (= 3 (:line-no (first heads))))
      (is (= 9 (:index (last heads))))
      (is (= 19 (:line-no (last heads))))
      (is (= #{"admin" "johny"} (into (hash-set) (mapv :user heads)))))))

(deftest ATX-to-blocks-test

  (testing "ATX blocks"

    (let [atx "datasets/sample.atx"
          datas (io/in! (io/lines {:eol "\r\n" :processor (comp vec blocks)})
                        atx)]
      (is (= 9 (count datas)))
      (is (= 1 (:at (first datas))))
      (is (= 4 (:rows (first datas))))
      (is (= 1 (:index (first datas))))
      (is (= [1 4] (:span (first datas))))
      (is (= 43 (:at (last datas))))
      (is (= 6 (:rows (last datas))))
      (is (= 9 (:index (last datas))))
      (is (= [43 48] (:span (last datas)))))))

(deftest TxLog-processor-test-headers-and-blocks-testing

  (testing "ALG+ATX, headers+blocks"

    (let [alg "datasets/sample.alg"
          atx "datasets/sample.atx"
          h+b (io/in! (ess.txl/tx-log {:processor vec})
                      {:alg alg :atx atx})
          hxb (io/in! (ess.txl/tx-log {:processor vec :strict true})
                      {:alg alg :atx atx})
          h+b2 ((ess.txl/tx-log {:processor vec})
                {:alg alg :atx atx})
          hxb2 (ess.txl/tx-log {:processor vec :strict true}
                               {:alg alg :atx atx})
          h+b3 ((ess.txl/tx-log {:processor vec})
                {:alg alg :atx atx})
          hxb3 (ess.txl/tx-log {:processor vec :strict true}
                               {:alg alg :atx atx})]
      (is (= h+b hxb))
      (is (= h+b h+b2))
      (is (= h+b h+b3))
      (is (= hxb hxb2))
      (is (= hxb hxb3))
      (is (= 9 (count h+b)))
      (is (= 1 (get-in (first h+b) [:head :at])))
      (is (= 1 (get-in (first h+b) [:data :at])))
      (is (true? (get-in (first h+b) [:ctrl :at-ok?])))
      (is (zero? (get-in (first h+b) [:ctrl :at-drift])))
      (is (= 4 (get-in (first h+b) [:head :rows])))
      (is (= 4 (get-in (first h+b) [:data :rows])))
      (is (true? (get-in (first h+b) [:ctrl :rows-ok?])))
      (is (zero? (get-in (first h+b) [:ctrl :rows-drift])))
      (is (= 3 (get-in (first h+b) [:head :line-no])))
      (is (= [1 4] (get-in (first h+b) [:data :span])))
      (is (= 1 (get-in (first h+b) [:head :index])
             (get-in (first h+b) [:data :index])))))

  (testing "ALG+ATX, headers+blocks - borked"

    (let [alg "datasets/sample.alg"
          atx "datasets/sample_borked.atx"
          h+b (io/in! (ess.txl/tx-log {:processor vec})
                      {:alg alg :atx atx})]
      (is (thrown? Exception
                   (io/in! (ess.txl/tx-log {:processor vec :strict true})
                           {:alg alg :atx atx})))
      (is (= 9 (count h+b)))))

  ;; Again, but directly -- no io/in!

  (testing "ALG+ATX, headers+blocks - no in!"

    (let [alg "datasets/sample.alg"
          atx "datasets/sample.atx"
          h+b ((ess.txl/tx-log {:processor vec}) {:alg alg :atx atx})
          hxb ((ess.txl/tx-log {:processor vec :strict true})
               {:alg alg :atx atx})]
      (is (= h+b hxb))
      (is (= 9 (count h+b)))
      (is (= 1 (get-in (first h+b) [:head :at])))
      (is (= 1 (get-in (first h+b) [:data :at])))
      (is (true? (get-in (first h+b) [:ctrl :at-ok?])))
      (is (zero? (get-in (first h+b) [:ctrl :at-drift])))
      (is (= 4 (get-in (first h+b) [:head :rows])))
      (is (= 4 (get-in (first h+b) [:data :rows])))
      (is (true? (get-in (first h+b) [:ctrl :rows-ok?])))
      (is (zero? (get-in (first h+b) [:ctrl :rows-drift])))
      (is (= 3 (get-in (first h+b) [:head :line-no])))
      (is (= [1 4] (get-in (first h+b) [:data :span])))
      (is (= 1 (get-in (first h+b) [:head :index])
             (get-in (first h+b) [:data :index])))))

  (testing "ALG+ATX, headers+blocks - borked - no in!"

    (let [alg "datasets/sample.alg"
          atx "datasets/sample_borked.atx"
          h+b ((ess.txl/tx-log {:processor vec}) {:alg alg :atx atx})]
      (is (thrown? Exception
                   ((ess.txl/tx-log {:processor vec :strict true})
                    {:alg alg :atx atx})))
      (is (= (count h+b) 9)))))

(deftest Drifts-test

  (letfn [(ok? [lines]
            (let [{:keys [at-ok? rows-ok?]} (-> lines meta :ctrl)]
              (if (and at-ok? rows-ok?)
                (= lines (-> lines meta :head :raw))
                (not= lines (-> lines meta :head :raw)))))
          (ok-proc [entries]
            (mapv ok? (ess.txl/correct-drifts entries)))]

    (testing "Drifts - no issues"
      (let [alg "datasets/sample.alg"
            atx "datasets/sample.atx"
            dzz (io/in! (ess.txl/tx-log {:processor ess.txl/drifts})
                        {:alg alg :atx atx})
            cln (io/in! (ess.txl/tx-log {:processor ok-proc})
                        {:alg alg :atx atx})
            lst (io/in! (ess.txl/tx-log
                          {:processor (comp vec ess.txl/list-drifts)})
                        {:alg alg :atx atx})]
        (is (= [1 9] (:processed dzz)))
        (is (= {} (:rows-drift dzz)))
        (is (= {} (:at-drift dzz)))
        (is (every? true? cln))
        (is (empty? lst))))

    (testing "Drifts - borked"

      (let [alg "datasets/sample.alg"
            btx "datasets/sample_borked.atx"
            dzz (io/in! (ess.txl/tx-log {:processor ess.txl/drifts})
                        {:alg alg :atx btx})
            drt (io/in! (ess.txl/tx-log {:processor ok-proc})
                        {:alg alg :atx btx})
            lst (io/in! (ess.txl/tx-log {:processor (comp vec ess.txl/list-drifts)})
                        {:alg alg :atx btx})]
        (is (= {6 1 7 -1} (:rows-drift dzz)))
        (is (= {7 1} (:at-drift dzz)))
        (is (= [1 9] (:processed dzz)))
        (is (every? true? drt))
        (is (= 2 (count lst)))))

    (testing "Drifts - correct-alg-drifts!"
      (let [alg "datasets/sample.alg"
            btx "datasets/sample_borked.atx"
            tmp (file-please!)
            fxd (.getCanonicalPath ^File tmp)
            dzz (io/in! (ess.txl/tx-log {:processor ess.txl/drifts})
                        {:alg alg :atx btx})
            _   (correct-alg-drifts! alg btx fxd)
            cln (io/in! (ess.txl/tx-log {:processor ess.txl/drifts})
                        {:alg fxd :atx btx})]
        (is (= {6 1 7 -1} (:rows-drift dzz)))
        (is (= {7 1} (:at-drift dzz)))
        (is (= [1 9] (:processed dzz)))
        (is (empty? (:rows-drift cln)))
        (is (empty? (:at-drift cln)))
        (is (= [1 9] (:processed cln)))
        (thanks-for-the-file! tmp)))

    ;; once again with a feeling... that's the name of the interceptor.
    (testing "Drifts - correct-alg-drifts! w/ interceptor"
      (let [alg "datasets/sample.alg"
            btx "datasets/sample_borked.atx"
            vol (volatile! nil)
            fln (fn [dem-lines]
                  (vreset! vol dem-lines)
                  dem-lines)
            tmp (file-please!)
            fxd (.getCanonicalPath ^File tmp)
            dzz (io/in! (ess.txl/tx-log {:processor ess.txl/drifts})
                        {:alg alg :atx btx})
            _   (correct-alg-drifts! alg btx fxd fln) ;; <--- magic here
            cln (io/in! (ess.txl/tx-log {:processor ess.txl/drifts})
                        {:alg fxd :atx btx})]
        (is (= {6 1 7 -1} (:rows-drift dzz)))
        (is (= {7 1} (:at-drift dzz)))
        (is (= [1 9] (:processed dzz)))
        (is (empty? (:rows-drift cln)))
        (is (empty? (:at-drift cln)))
        (is (= [1 9] (:processed cln)))
        (is (= 10 (count @vol))) ;; <--- includes header.
        (thanks-for-the-file! tmp)))))

(deftest Predicates-test

  (testing "Interval matches."

    (let [alg "datasets/sample.alg"
          atx "datasets/sample.atx"
          in? (between? "2011-01-20T07:15:00.000Z"
                        "2011-01-26T10:30:00.000Z")
          res (io/in! (ess.txl/tx-log {:processor (partial filterv in?)})
                      {:alg alg :atx atx})]
      (is (= 2 (count res)))
      (is (= [15 19] (mapv (comp :at :head) res)))))

  (testing "User matches."
    (let [alg "datasets/sample.alg"
          atx "datasets/sample.atx"
          in? (user-matches? #"(?xi)(?:johny)")
          res (io/in! (ess.txl/tx-log {:processor (partial filterv in?)})
                      {:alg alg :atx atx})]
      (is (= 2 (count res)))
      (is (= [15 31] (mapv (comp :at :head) res)))))

  (testing "Some Data matches."
    (let [alg "datasets/sample.alg"
          atx "datasets/sample.atx"
          in? (data-matches? #"(?xi)\"(?:FY18)\"")
          res (io/in! (ess.txl/tx-log {:processor (partial filterv in?)})
                      {:alg alg :atx atx})]
      (is (= 1 (count res)))
      (is (= [19] (mapv (comp :at :head) res)))
      (is (= [11] (mapv (comp :line-no :head) res)))
      (is (= [[19 23]] (mapv (comp :span :data) res))))))

(deftest Printable-testing
  (testing "Expected form"
    (let [alg "datasets/sample.alg"
          atx "datasets/sample.atx"
          res (io/in! (ess.txl/tx-log {:processor (comp vec printable)})
                      {:alg alg :atx atx})]
      (is (= 75 (count res)))))

  (testing "Transducer form"
    (let [alg "datasets/sample.alg"
          atx "datasets/sample.atx"
          xf  (printable)
          res (io/in! (ess.txl/tx-log {:processor (partial into [] xf)})
                      {:alg alg :atx atx})]
      (is (= 75 (count res))))))
