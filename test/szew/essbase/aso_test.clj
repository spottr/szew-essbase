; Copyright (c) Sławek Gwizdowski
;
; Permission is hereby granted, free of charge, to any person obtaining
; a copy of this software and associated documentation files (the "Software"),
; to deal in the Software without restriction, including without limitation
; the rights to use, copy, modify, merge, publish, distribute, sublicense,
; and/or sell copies of the Software, and to permit persons to whom the
; Software is furnished to do so, subject to the following conditions:
;
; The above copyright notice and this permission notice shall be included
; in all copies or substantial portions of the Software.
;
; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
; OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
; FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
; THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
; LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
; FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
; IN THE SOFTWARE.
;
(ns ^{:author "Sławek Gwizdowski"
      :doc "Essbase ASO export - tests."}
 szew.essbase.aso-test
 (:require
   [szew.essbase.aso
    :refer [members cells sniff-dimensions sniff-unknown dump->tsv]]
   [clojure.test :refer [deftest testing is]]
   [orchestra.spec.test :as st]
   [szew.io :as io]
   [szew.helpers-test
    :refer [file-please! thanks-for-the-file!]]))

(st/instrument)

;; ## Dimension mapper

(defn m->d
  "Get member, return dimension, test data edition."
  [^String m]
  (let [period   #"^(?:M00|Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec)$"
        version  #"^(?:No_Version|Work)$"
        currency #"^(?:GBP|USD)$"
        year     #"^(?:No_Year|FY\d\d)$"
        account  #"^(?:No_Account|AC\.[\d\w]*)$"
        project  #"^(?:No_Project|PR\.[\.\d\w]*)$"
        entity   #"^(?:No_Entity|CC\.[\d\w]*)$"
        scenario #"^(?:No_Scenario|Estimate)$"]
    (cond
      (re-matches account m)  :account
      (re-matches entity m)   :entity
      (re-matches project m)  :project
      (re-matches period m)   :period
      (re-matches version m)  :version
      (re-matches currency m) :currency
      (re-matches year m)     :year
      (re-matches scenario m) :scenario)))

(deftest Input-processing-testing

  (testing "Members"
    (let [sauce "datasets/ASO_sanitized_sample.txt"
          mset  (io/in! (members) sauce)
          mset2 ((members) sauce)
          mset3 (members {} sauce)]
      (is (= 35 (count mset)))
      (is (true? (contains? mset "AC.186044")))
      (is (true? (contains? mset "PR.ITSDPCUMN")))
      (is (true? (contains? mset "CC.OR7140")))
      (is (= mset mset2))
      (is (= mset mset3))))

  (testing "Cells"
    (let [sauce "datasets/ASO_sanitized_sample.txt"
          cvec  (io/in! (cells {:m->d m->d}) sauce)
          cvec2 ((cells {:m->d m->d}) sauce)
          cvec3 (cells {:m->d m->d} sauce)
          f-pov (first cvec)
          l-pov (last cvec)]
      (is (= 20 (count cvec)))
      (is (= f-pov
             [{:currency "GBP" :project "PR.CL37188S"
               :entity "CC.U686N2" :year "FY05" :scenario "Estimate"
               :version "Work" :period "M00" :account "AC.186044"}
              "58"]))
      (is (= {:line 0
              :source sauce
              :row ["AC.186044" "M00" "Work" "Estimate"
                    "FY05" "CC.U686N2" "PR.CL37188S" "GBP" "58"]}
             (meta f-pov)))
      (is (= l-pov
             [{:currency "GBP" :project "PR.0635FO71A1"
               :entity "CC.OR7140" :year "FY05" :scenario "Estimate"
               :version "Work" :period "M00" :account "AC.186460"}
              "-11"]))
      (is (= {:line 19 :source sauce :row ["CC.OR7140" "-11"]}
             (meta l-pov)))
      (is (= cvec cvec2))
      (is (= cvec cvec3))))

  (testing "Cells - error reporting"
    (is (thrown? Exception
                 (io/in! (cells) "datasets/ASO_sanitized_sample.txt")))
    (is (thrown? Exception
                 (io/in! (cells {:m->d m->d})
                         "datasets/ASO_unknown_sample.txt")))
    (let [ex (try (io/in! (cells) "datasets/ASO_sanitized_sample.txt")
                  (catch Exception ex ex))]
      (is (= 0 (-> ex ex-data :line)))
      (is (= 0 (-> ex ex-data :cell meta :line)))
      (is (= "datasets/ASO_sanitized_sample.txt"
             (-> ex ex-data :cell meta :source))))))

(deftest Member-helpers-and-company-testing

  (testing "Sniffing the unknown"
    (let [all-ok (io/in! (members {:processor (comp vec (sniff-unknown m->d))})
                         "datasets/ASO_sanitized_sample.txt")
          all-nu (io/in! (members {:processor (comp vec (sniff-unknown {}))})
                         "datasets/ASO_sanitized_sample.txt")
          some-u (io/in! (members {:processor (comp vec (sniff-unknown m->d))})
                         "datasets/ASO_unknown_sample.txt")]
      (is (= 0 (count all-ok)))
      (is (= 35 (count all-nu)))
      (is (= #{"RRU" "XX.DS37109S" "ZZ.186460" "YY.8M7911"}
             (into (hash-set) some-u)))
      (is (= 4 (count some-u))))))

(deftest Cells-helpers-and-company-testing
  (testing "Sneefing the dimensions up"
    (let [cs (cells {:processor sniff-dimensions :m->d m->d})]
      (is (= #{:account :period :version :scenario :year :entity :project
               :currency}
             (cs "datasets/ASO_sanitized_sample.txt"))))))

(deftest ASO-dump-to-tsv-testing
  (let [tmp    (file-please!)
        _      (dump->tsv m->d
                          [:account :period :version :scenario
                           :year :entity :project :currency]
                          tmp
                          "datasets/ASO_sanitized_sample.txt")
        master (io/in! (io/csv {:processor vec :delimiter \tab})
                       "datasets/ASO_sanitized_output.txt")
        newbie (io/in! (io/csv {:processor vec :delimiter \tab}) tmp)]
    (is (= master newbie))
    (thanks-for-the-file! tmp)))
