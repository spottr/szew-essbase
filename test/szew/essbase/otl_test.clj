; Copyright (c) Sławek Gwizdowski
;
; Permission is hereby granted free of charge to any person obtaining
; a copy of this software and associated documentation files (the "Software")
; to deal in the Software without restriction including without limitation
; the rights to use copy modify merge publish distribute sublicense
; and/or sell copies of the Software and to permit persons to whom the
; Software is furnished to do so subject to the following conditions:
;
; The above copyright notice and this permission notice shall be included
; in all copies or substantial portions of the Software.
;
; THE SOFTWARE IS PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND EXPRESS
; OR IMPLIED INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY
; FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
; THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM DAMAGES OR OTHER
; LIABILITY WHETHER IN AN ACTION OF CONTRACT TORT OR OTHERWISE ARISING
; FROM OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
; IN THE SOFTWARE.
;
(ns ^{:author "Sławek Gwizdowski"
      :doc "Essbase XML Outline export - tests."}
 szew.essbase.otl-test
 (:require
   [szew.essbase.otl :as otl
    :refer [list-content-tags list-dimensions! extract-dimensions!
            member-seq member-walk member-name-set member-lut!
            member-lut-package! non-attr-dim cube
            smart-lists]]
   [szew.essbase.otl.member :refer [map->OutlineMember]]
   [szew.essbase.otl.dimension :refer [map->OutlineDimension]]
   [szew.io :as io]
   [clojure.test :refer [deftest testing is]]
   [orchestra.spec.test :as st]
   [clojure.zip :as zip]))

(st/instrument)

(deftest Dimension-parsing-testing

  (testing "Misc Helpers testing"
    (is (= [:Dimension :Dimension :Dimension :Dimension
            :Dimension :Dimension :Dimension :Dimension
            :Dimension :Dimension :cube :smartLists]
           (io/in! (io/xml {:processor list-content-tags})
                   "datasets/sample_basic.xml"))))

  (testing "Dimension listing"
    (let [dims (list-dimensions! "datasets/sample_basic.xml")]
      (is (= 10 (count dims)))
      (is (= ["Year" "Measures" "Product" "Market" "Scenario"
              "Caffeinated" "Ounces" "Pkg Type" "Population" "Intro Date"]
             (mapv :name dims)))))

  (testing "Dimension processing"
    (let [dims         (extract-dimensions! "datasets/sample_basic.xml")
          names-counts (mapv (juxt :name :member-count) dims)]
      (is (= [["Year" 16]
              ["Measures" 16]
              ["Product" 21]
              ["Market" 24]
              ["Scenario" 4]
              ["Caffeinated" 2]
              ["Ounces" 4]
              ["Pkg Type" 2]
              ["Population" 14]
              ["Intro Date" 7]]
             names-counts))

      (let [year (first dims)
            qtr1 (-> dims first :members first)
            jan  (-> dims first :members first :members first)]
        (is (= (map->OutlineDimension
                 {:attribute-nodes #{}
                  :contained [:Generations :Member :Member :Member :Member]
                  :csversion "4.0"
                  :data-storage "DynamicCalc"
                  :density "Dense"
                  :dimension-type "Time"
                  :generations {1 "History" 2 "Quarter" 3 "Months"}
                  :hierarchy-type "Stored"
                  :is-compression-dimension false
                  :levels {}
                  :member-count 16
                  :members nil
                  :misc {}
                  :name "Year"
                  :tag :Dimension})
               (assoc year :members nil)))
        (is (= (map->OutlineMember
                 {:alias "Quarter1"
                  :aliases {"Long Names" "Quarter1"}
                  :attributes {}
                  :consolidation "+"
                  :contained [:Alias :Member :Member :Member]
                  :data-storage "DynamicCalc"
                  :dimension "Year"
                  :generation 1
                  :hierarchy-type "Disabled"
                  :level0? false
                  :members nil
                  :misc {}
                  :name "Qtr1"
                  :parent "Year"
                  :path ["Year"]
                  :shared-member false
                  :tag :Member
                  :two-pass-calc false
                  :uda #{}})
               (assoc qtr1 :members nil)))
        (is (= (map->OutlineMember
                 {:alias "January"
                  :aliases {"Long Names" "January"}
                  :attributes {}
                  :consolidation "+"
                  :contained [:Alias]
                  :data-storage "StoreData"
                  :dimension "Year"
                  :generation 2
                  :level0? true
                  :members nil
                  :misc {}
                  :name "Jan"
                  :parent "Qtr1"
                  :path ["Year" "Qtr1"]
                  :shared-member false
                  :tag :Member
                  :two-pass-calc false
                  :uda #{}})
               jan)))

      (let [measures (second dims)
            cogs (-> measures :members first
                     :members first
                     :members second)
            oinv (->> (member-seq (second dims))
                      (filter (comp (partial = "Opening Inventory") :name))
                      first)]
        (is (= (map->OutlineMember
                {:alias "Cost of Goods Sold"
                 :aliases {"Long Names" "Cost of Goods Sold"}
                 :attributes {}
                 :consolidation "-"
                 :contained [:Alias]
                 :data-storage "StoreData"
                 :dimension "Measures"
                 :generation 3
                 :level0? true
                 :members nil
                 :misc {}
                 :name "COGS"
                 :parent "Margin"
                 :path ["Measures" "Profit" "Margin"]
                 :shared-member false
                 :tag :Member
                 :two-pass-calc false
                 :uda #{}
                 :variance-reporting "Expense"})
               (assoc cogs :members nil)))
        (is (= (map->OutlineMember
                {:alias ""
                 :aliases {}
                 :attributes {}
                 :consolidation "+"
                 :contained []
                 :data-storage "StoreData"
                 :dimension "Measures"
                 :generation 2
                 :hierarchy-type nil
                 :level0? true
                 :member-formula "IF(NOT @ISMBR(Jan))\"Opening Inventory\"=@PRIOR(\"Ending Inventory\");   ENDIF; \"Ending Inventory\"=\"Opening Inventory\"+Additions-Sales;"
                 :members nil
                 :misc {}
                 :name "Opening Inventory"
                 :parent "Inventory"
                 :path ["Measures" "Inventory"]
                 :shared-member false
                 :storage-category nil
                 :tag :Member
                 :time-balance "First"
                 :two-pass-calc false
                 :uda #{}
                 :variance-reporting "Expense"})
             oinv)))

      (let [product (nth dims 2)
            colas (-> product :members first)
            cola (-> product :members first :members first)]
        (is (= (map->OutlineDimension
                {:attribute-nodes #{"Caffeinated" "Intro Date" "Ounces" "Pkg Type"}
                 :attribute-type nil
                 :contained [:AttributeDimension
                             :AttributeDimension
                             :AttributeDimension
                             :AttributeDimension
                             :Generations
                             :Levels
                             :Member
                             :Member
                             :Member
                             :Member
                             :Member]
                 :csversion "4.0"
                 :data-storage "StoreData"
                 :density "Sparse"
                 :dimension-type "Standard"
                 :generations {2 "Category", 3 "Product SKU"}
                 :hierarchy-type "Stored"
                 :is-compression-dimension false
                 :levels {0 "SKU", 1 "Family"}
                 :member-count 21
                 :members nil
                 :misc {}
                 :name "Product"
                 :tag :Dimension})
               (assoc product :members nil)))
        (is (= (map->OutlineMember
                {:alias "Colas"
                 :aliases {"Default" "Colas"}
                 :attributes {}
                 :consolidation "+"
                 :contained [:Alias :Member :Member :Member]
                 :data-storage "StoreData"
                 :dimension "Product"
                 :generation 1
                 :hierarchy-type "Disabled"
                 :level0? false
                 :members nil
                 :name "100"
                 :misc {}
                 :parent "Product"
                 :path ["Product"]
                 :shared-member false
                 :tag :Member
                 :two-pass-calc false
                 :uda #{}})
               (assoc colas :members nil)))
        (is (= (map->OutlineMember
                {:alias "Cola"
                 :aliases {"Default" "Cola"}
                 :attributes {"Caffeinated" "True"
                              "Intro Date" "03-25-1996"
                              "Ounces" "12"
                              "Pkg Type" "Can"}
                 :consolidation "+"
                 :contained [:AttributeMember
                             :AttributeMember
                             :AttributeMember
                             :AttributeMember
                             :Alias]
                 :data-storage "StoreData"
                 :dimension "Product"
                 :generation 2
                 :level0? true
                 :members nil
                 :misc {}
                 :name "100-10"
                 :parent "100"
                 :path ["Product" "100"]
                 :shared-member false
                 :tag :Member
                 :two-pass-calc false
                 :uda #{}})
               cola)))

      (let [pkg-type (nth dims 7)
            can (-> pkg-type :members last)]
        (is (= (map->OutlineDimension
                {:attribute-nodes #{}
                 :attribute-type "Text"
                 :contained [:Member :Member]
                 :csversion "4.0"
                 :data-storage "DynamicCalc"
                 :density "Sparse"
                 :dimension-type "Attribute"
                 :generations {}
                 :hierarchy-type "Stored"
                 :is-compression-dimension false
                 :levels {}
                 :member-count 2
                 :misc {}
                 :name "Pkg Type"
                 :storage-category "Attribute"
                 :tag :Dimension})
               (assoc pkg-type :members nil)))
        (is (= (map->OutlineMember
                {:alias ""
                 :aliases {}
                 :attributes {}
                 :consolidation "~"
                 :contained []
                 :data-storage "DynamicCalc"
                 :dimension "Pkg Type"
                 :generation 1
                 :hierarchy-type "Disabled"
                 :level0? true
                 :misc {}
                 :name "Can"
                 :parent "Pkg Type"
                 :path ["Pkg Type"]
                 :shared-member false
                 :storage-category "Attribute"
                 :tag :Member
                 :two-pass-calc false
                 :uda #{}})
               can)))))

  (testing "Filtering members"
    (let [year    (extract-dimensions!
                    (comp (partial = "Year") :name)
                    first
                    "datasets/sample_basic.xml")
          wanted? (comp #{"Jan" "Jun" "Dec"} :name)
          result  (filterv wanted? (member-seq year))]
      (is (= #{"Jan" "Jun" "Dec"} (set (map :name result))))))

  (testing "Visiting members"
    (let [wanted?  (comp #{"Jan" "Jun" "Dec"} :name)
          mark     (fn [loc]
                     (if (wanted? (zip/node loc))
                       (-> loc (zip/edit assoc :X true)) loc))
          result   (extract-dimensions!
                     (comp (partial = "Year") :name)
                     (comp (partial filterv wanted?)
                           member-seq
                           (partial member-walk mark)
                           first)
                     "datasets/sample_basic.xml")]
      (is (= #{"Jan" "Jun" "Dec"} (set (map :name result))))
      (is (every? true? (map :X result)))))

  (testing "Member set"
    (let [dims   (extract-dimensions! "datasets/sample_basic.xml")]
      ;; Dimension + Members
      (is (= [["Year" 17]
              ["Measures" 17]
              ["Product" 19] ;; 3 shared members
              ["Market" 25]
              ["Scenario" 5]
              ["Caffeinated" 3]
              ["Ounces" 5]
              ["Pkg Type" 3]
              ["Population" 15]
              ["Intro Date" 8]]
             (mapv (juxt :name (comp count member-name-set)) dims)))
      ;; Just Members, but with shared.
      (is (= [["Year" 16]
              ["Measures" 16]
              ["Product" 21] ;; 3 shared members
              ["Market" 24]
              ["Scenario" 4]
              ["Caffeinated" 2]
              ["Ounces" 4]
              ["Pkg Type" 2]
              ["Population" 14]
              ["Intro Date" 7]]
             (mapv (juxt :name :member-count) dims)))))

  (testing "Member LUT"
    (let [noms (extract-dimensions!
                 (comp set
                       (partial map :name)
                       (partial mapcat (partial drop 1))
                       (partial map member-seq))
                 "datasets/sample_basic.xml")
          ;; get the member count, no dimensions (so dec)
          uniq (extract-dimensions!
                 (comp
                   (partial reduce + 0)
                   (partial map (comp dec count))
                   (partial map member-name-set))
                 "datasets/sample_basic.xml")
          lut  (member-lut! "datasets/sample_basic.xml")
          pkg  (member-lut-package! "datasets/sample_basic.xml")]
      ;; 110 total, 3 shared, 107 distinct names
      (is (= 107 (count noms) uniq))
      (is (every? string? (map lut noms)))
      (is (nil? (lut "Non-existent Member Name!")))
      (is (= :not-found (lut "Non-existent Member Name!" :not-found)))
      (is (= (-> lut meta :lut)
             (-> pkg :m->d meta :lut)))
      (is (= 5 (:dim-count pkg) (:data-dims (meta lut))))
      (is (= 10 (:total-dims (meta lut))))
      (is (= 5 (:attr-dims (meta lut)))))
    ;; now with more feeling
    (let [lut  (member-lut! non-attr-dim "datasets/sample_basic.xml")
          pkg  (member-lut-package! non-attr-dim "datasets/sample_basic.xml")]
      (is (= (-> lut meta :lut) (-> pkg :m->d meta :lut)))
      (is (= 5 (:dim-count pkg) (:data-dims (meta lut))))
      (is (= 5 (:total-dims (meta lut))))
      (is (= 0 (:attr-dims (meta lut)))))))

(deftest Testing-cube-parsing
  (let [cube-data (io/in! (io/xml {:processor cube})
                          "datasets/sample_basic.xml")]
    (is (= {:alias-tables #{"Default" "Long Names"}
            :application-settings {:cube-type "BSO"}
            :attribute-calculation-settings
            {:bso-attribute-calcs
             {:avg "Avg"
              :count "Count"
              :dimension-name "Attribute Calculations"
              :false "False"
              :max "Max"
              :min "Min"
              :range-name "Tops of Ranges"
              :sum "Sum"
              :true "True"}}
            :attribute-settings
            {:prefix-suffix-format "Parent" :prefix-suffix-value "Prefix"}
            :contained
            [:applicationSettings
             :databaseSettings
             :aliasTables
             :dynamicTimeSeriesSettings
             :attributeSettings
             :attributeCalculationSettings]
            :database-settings
            {:date-format "yyyy-mm-dd"
             :implied-shared-setting "default On"
             :locale "English_UnitedStates.Latin1@Binary"}
            :default-alias-table "Default"
            :dynamic-time-series-settings
            {:H-T-D {:enabled "Y" :generation "1"}
             :Q-T-D {:enabled "Y" :generation "2"}}
            :tag :cube}
           cube-data))))

(deftest Testing-smart-lists-parsing
  (let [sl-data (io/in! (io/xml {:processor smart-lists})
                        "datasets/sample_basic.xml")]
    (is (= :not-implemented sl-data))))

;; 11.2 encore

(deftest Dimension-parsing-testing-11-2

  (testing "Misc Helpers testing"
    (is (= (-> [:Dimension :Dimension :Dimension :Dimension
                :Dimension :Dimension :Dimension :Dimension
                :Dimension :Dimension :cube :smartLists :Validation]
               sort vec)
           (io/in! (io/xml {:processor (comp vec sort list-content-tags)})
                   "datasets/sample_basic_11_2.xml"))))

  (testing "Dimension listing"
    (let [dims (list-dimensions! "datasets/sample_basic_11_2.xml")]
      (is (= 10 (count dims)))
      (is (= ["Year" "Measures" "Product" "Market" "Scenario"
              "Caffeinated" "Ounces" "Pkg Type" "Population" "Intro Date"]
             (mapv :name dims)))))

  (testing "Dimension processing"
    (let [dims         (extract-dimensions! "datasets/sample_basic_11_2.xml")
          names-counts (mapv (juxt :name :member-count) dims)]
      (is (= [["Year" 16]
              ["Measures" 16]
              ["Product" 21]
              ["Market" 24]
              ["Scenario" 4]
              ["Caffeinated" 2]
              ["Ounces" 4]
              ["Pkg Type" 2]
              ["Population" 14]
              ["Intro Date" 7]]
             names-counts))

      (let [year (first dims)
            qtr1 (-> dims first :members first)
            jan  (-> dims first :members first :members first)]
        (is (= (map->OutlineDimension
                {:attribute-nodes #{}
                 :contained [:Generations :MemberPropertiesInfo
                             :Member :Member :Member :Member]
                 :csversion "4.0"
                 :data-storage "DynamicCalc"
                 :density "Dense"
                 :dimension-type "Time"
                 :generations {1 "History" 2 "Quarter" 3 "Months"}
                 :hierarchy-type "Stored"
                 :is-compression-dimension false
                 :levels {}
                 :member-count 16
                 :members nil
                 :misc {}
                 :name "Year"
                 :tag :Dimension})
               (assoc year :members nil :misc {}))) ;; TODO: 11.2 new things
        (is (= (map->OutlineMember
                {:alias "Quarter1"
                 :aliases {"Long Names" "Quarter1"}
                 :attributes {}
                 :consolidation "+"
                 :contained [:Alias :Member :Member :Member]
                 :data-storage "DynamicCalc"
                 :dimension "Year"
                 :generation 1
                 :hierarchy-type "Disabled"
                 :level0? false
                 :members nil
                 :misc {}
                 :name "Qtr1"
                 :parent "Year"
                 :path ["Year"]
                 :shared-member false
                 :tag :Member
                 :two-pass-calc false
                 :uda #{}})
               (assoc qtr1 :members nil :misc {}))) ;; TODO: 11.2 new things
        (is (= (map->OutlineMember
                {:alias "January"
                 :aliases {"Long Names" "January"}
                 :attributes {}
                 :consolidation "+"
                 :contained [:Alias]
                 :data-storage "StoreData"
                 :dimension "Year"
                 :generation 2
                 :level0? true
                 :members nil
                 :misc {}
                 :name "Jan"
                 :parent "Qtr1"
                 :path ["Year" "Qtr1"]
                 :shared-member false
                 :tag :Member
                 :two-pass-calc false
                 :uda #{}})
               jan)))

      (let [measures (second dims)
            cogs (-> measures :members first
                     :members first
                     :members second)
            oinv (->> (member-seq (second dims))
                      (filter (comp (partial = "Opening Inventory") :name))
                      first)]
        (is (= (map->OutlineMember
                {:alias "Cost of Goods Sold"
                 :aliases {"Long Names" "Cost of Goods Sold"}
                 :attributes {}
                 :consolidation "-"
                 :contained [:Alias]
                 :data-storage "StoreData"
                 :dimension "Measures"
                 :generation 3
                 :level0? true
                 :members nil
                 :misc {}
                 :name "COGS"
                 :parent "Margin"
                 :path ["Measures" "Profit" "Margin"]
                 :shared-member false
                 :tag :Member
                 :two-pass-calc false
                 :uda #{}
                 :variance-reporting "Expense"})
               (assoc cogs :members nil)))
        (is (= (map->OutlineMember
                {:alias ""
                 :aliases {}
                 :attributes {}
                 :consolidation "+"
                 :contained [:MemberFormula]
                 :data-storage "StoreData"
                 :dimension "Measures"
                 :generation 2
                 :hierarchy-type nil
                 :level0? true
                 :member-formula "\tIF(NOT @ISMBR(Jan))\"Opening Inventory\"=@PRIOR(\"Ending Inventory\"); \n\nENDIF; \"Ending Inventory\"=\"Opening Inventory\"+Additions-Sales;\n"
                 :members nil
                 :misc {}
                 :name "Opening Inventory"
                 :parent "Inventory"
                 :path ["Measures" "Inventory"]
                 :shared-member false
                 :storage-category nil
                 :tag :Member
                 :time-balance "First"
                 :two-pass-calc false
                 :uda #{}
                 :variance-reporting "Expense"})
               oinv)))

      (let [product (nth dims 2)
            colas (-> product :members first)
            cola (-> product :members first :members first)]
        (is (= (map->OutlineDimension
                {:attribute-nodes #{"Caffeinated" "Intro Date" "Ounces" "Pkg Type"}
                 :attribute-type nil
                 :contained [:AttributeDimension
                             :AttributeDimension
                             :AttributeDimension
                             :AttributeDimension
                             :Generations
                             :Levels
                             :MemberPropertiesInfo
                             :Member
                             :Member
                             :Member
                             :Member
                             :Member]
                 :csversion "4.0"
                 :data-storage "StoreData"
                 :density "Sparse"
                 :dimension-type "Standard"
                 :generations {2 "Category", 3 "Product SKU"}
                 :hierarchy-type "Stored"
                 :is-compression-dimension false
                 :levels {0 "SKU", 1 "Family"}
                 :member-count 21
                 :members nil
                 :misc {}
                 :name "Product"
                 :tag :Dimension})
               (assoc product :members nil :misc {}))) ;; TODO: 11.2 new things
        (is (= (map->OutlineMember
                {:alias "Colas"
                 :aliases {"Default" "Colas"}
                 :attributes {}
                 :consolidation "+"
                 :contained [:Alias :Member :Member :Member]
                 :data-storage "StoreData"
                 :dimension "Product"
                 :generation 1
                 :hierarchy-type "Disabled"
                 :level0? false
                 :members nil
                 :name "100"
                 :misc {}
                 :parent "Product"
                 :path ["Product"]
                 :shared-member false
                 :tag :Member
                 :two-pass-calc false
                 :uda #{}})
               (assoc colas :members nil)))
        (is (= (map->OutlineMember
                {:alias "Cola"
                 :aliases {"Default" "Cola"}
                 :attributes {"Caffeinated" "True"
                              "Intro Date" "03-25-1996"
                              "Ounces" "12"
                              "Pkg Type" "Can"}
                 :consolidation "+"
                 :contained [:AttributeMember
                             :AttributeMember
                             :AttributeMember
                             :AttributeMember
                             :Alias]
                 :data-storage "StoreData"
                 :dimension "Product"
                 :generation 2
                 :level0? true
                 :members nil
                 :misc {}
                 :name "100-10"
                 :parent "100"
                 :path ["Product" "100"]
                 :shared-member false
                 :tag :Member
                 :two-pass-calc false
                 :uda #{}})
               cola)))

      (let [pkg-type (nth dims 7)
            can (-> pkg-type :members last)]
        (is (= (map->OutlineDimension
                {:attribute-nodes #{}
                 :attribute-type "Text"
                 :contained [:MemberPropertiesInfo :Member :Member]
                 :csversion "4.0"
                 :data-storage "DynamicCalc"
                 :density "Sparse"
                 :dimension-type "Attribute"
                 :generations {}
                 :hierarchy-type "Stored"
                 :is-compression-dimension false
                 :levels {}
                 :member-count 2
                 :misc {}
                 :name "Pkg Type"
                 :storage-category "Attribute"
                 :tag :Dimension})
               (assoc pkg-type :members nil :misc {}))) ;;TODO: 11.2 new things
        (is (= (map->OutlineMember
                {:alias ""
                 :aliases {}
                 :attributes {}
                 :consolidation "~"
                 :contained []
                 :data-storage "DynamicCalc"
                 :dimension "Pkg Type"
                 :generation 1
                 :hierarchy-type "Disabled"
                 :level0? true
                 :misc {}
                 :name "Can"
                 :parent "Pkg Type"
                 :path ["Pkg Type"]
                 :shared-member false
                 :storage-category "Attribute"
                 :tag :Member
                 :two-pass-calc false
                 :uda #{}})
               can)))))

  (testing "Filtering members"
    (let [year    (extract-dimensions!
                   (comp (partial = "Year") :name)
                   first
                   "datasets/sample_basic_11_2.xml")
          wanted? (comp #{"Jan" "Jun" "Dec"} :name)
          result  (filterv wanted? (member-seq year))]
      (is (= #{"Jan" "Jun" "Dec"} (set (map :name result))))))

  (testing "Visiting members"
    (let [wanted?  (comp #{"Jan" "Jun" "Dec"} :name)
          mark     (fn [loc]
                     (if (wanted? (zip/node loc))
                       (-> loc (zip/edit assoc :X true)) loc))
          result   (extract-dimensions!
                    (comp (partial = "Year") :name)
                    (comp (partial filterv wanted?)
                          member-seq
                          (partial member-walk mark)
                          first)
                    "datasets/sample_basic_11_2.xml")]
      (is (= #{"Jan" "Jun" "Dec"} (set (map :name result))))
      (is (every? true? (map :X result)))))

  (testing "Member set"
    (let [dims   (extract-dimensions! "datasets/sample_basic_11_2.xml")]
      ;; Dimension + Members
      (is (= [["Year" 17]
              ["Measures" 17]
              ["Product" 19] ;; 3 shared members
              ["Market" 25]
              ["Scenario" 5]
              ["Caffeinated" 3]
              ["Ounces" 5]
              ["Pkg Type" 3]
              ["Population" 15]
              ["Intro Date" 8]]
             (mapv (juxt :name (comp count member-name-set)) dims)))
      ;; Just Members, but with shared.
      (is (= [["Year" 16]
              ["Measures" 16]
              ["Product" 21] ;; 3 shared members
              ["Market" 24]
              ["Scenario" 4]
              ["Caffeinated" 2]
              ["Ounces" 4]
              ["Pkg Type" 2]
              ["Population" 14]
              ["Intro Date" 7]]
             (mapv (juxt :name :member-count) dims)))))

  (testing "Member LUT"
    (let [noms (extract-dimensions!
                (comp set
                      (partial map :name)
                      (partial mapcat (partial drop 1))
                      (partial map member-seq))
                "datasets/sample_basic_11_2.xml")
          ;; get the member count, no dimensions (so dec)
          uniq (extract-dimensions!
                (comp
                 (partial reduce + 0)
                 (partial map (comp dec count))
                 (partial map member-name-set))
                "datasets/sample_basic_11_2.xml")
          lut  (member-lut! "datasets/sample_basic_11_2.xml")
          pkg  (member-lut-package! "datasets/sample_basic_11_2.xml")]
      ;; 110 total, 3 shared, 107 distinct names
      (is (= 107 (count noms) uniq))
      (is (every? string? (map lut noms)))
      (is (nil? (lut "Non-existent Member Name!")))
      (is (= :not-found (lut "Non-existent Member Name!" :not-found)))
      (is (= (-> lut meta :lut)
             (-> pkg :m->d meta :lut)))
      (is (= 5 (:dim-count pkg) (:data-dims (meta lut))))
      (is (= 10 (:total-dims (meta lut))))
      (is (= 5 (:attr-dims (meta lut)))))
    ;; now with more feeling
    (let [lut  (member-lut! non-attr-dim "datasets/sample_basic_11_2.xml")
          pkg  (member-lut-package! non-attr-dim "datasets/sample_basic_11_2.xml")]
      (is (= (-> lut meta :lut) (-> pkg :m->d meta :lut)))
      (is (= 5 (:dim-count pkg) (:data-dims (meta lut))))
      (is (= 5 (:total-dims (meta lut))))
      (is (= 0 (:attr-dims (meta lut)))))))

(deftest Testing-cube-parsing-11-2
  (let [cube-data (io/in! (io/xml {:processor cube})
                          "datasets/sample_basic_11_2.xml")]
    (is (= {:alias-tables #{"Default" "Long Names"}
            :application-settings {:cube-type "BSO"}
            :attribute-calculation-settings
            {:bso-attribute-calcs
             {:avg "Avg"
              :count "Count"
              :dimension-name "Attribute Calculations"
              :false "False"
              :max "Max"
              :min "Min"
              :range-name "Tops of Ranges"
              :sum "Sum"
              :true "True"}}
            :attribute-settings
            {:prefix-suffix-format "Parent" :prefix-suffix-value "Prefix"}
            :contained
            [:applicationSettings
             :databaseSettings
             :aliasTables
             :dynamicTimeSeriesSettings
             :attributeSettings
             :attributeCalculationSettings]
            :database-settings
            {:date-format "yyyy-mm-dd"
             :implied-shared-setting "default On"
             :locale "English_UnitedStates.Latin1@Binary"}
            :default-alias-table "Default"
            :dynamic-time-series-settings
            {:H-T-D {:enabled "Y" :generation "1"}
             :Q-T-D {:enabled "Y" :generation "2"}}
            :tag :cube}
           cube-data))))

(deftest Testing-smart-lists-parsing-11-2
  (let [sl-data (io/in! (io/xml {:processor smart-lists})
                        "datasets/sample_basic_11_2.xml")]
    (is (= :not-implemented sl-data))))