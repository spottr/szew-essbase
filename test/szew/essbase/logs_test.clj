; Copyright (c) Sławek Gwizdowski
;
; Permission is hereby granted, free of charge, to any person obtaining
; a copy of this software and associated documentation files (the "Software"),
; to deal in the Software without restriction, including without limitation
; the rights to use, copy, modify, merge, publish, distribute, sublicense,
; and/or sell copies of the Software, and to permit persons to whom the
; Software is furnished to do so, subject to the following conditions:
;
; The above copyright notice and this permission notice shall be included
; in all copies or substantial portions of the Software.
;
; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
; OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
; FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
; THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
; LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
; FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
; IN THE SOFTWARE.
;
(ns ^{:author "Sławek Gwizdowski"
      :doc "Essbase application log and MaxL spool - tests."}
 szew.essbase.logs-test
 (:require
   [szew.essbase.logs :as ess.logs :refer [app-log maxl-spool]]
   [szew.io :as io]
   [clojure.test :refer [deftest testing is]]
   [orchestra.spec.test :as st]))

(st/instrument)

(deftest Reading-Sample-Basic-log-from-datasets-test

  (testing "Sample.Basic"

    (let [log  "datasets/sample_basic.log"
          all  (io/in! (app-log {:processor vec}) log)
          all2 ((app-log {:processor vec}) log)
          all3 (app-log {:processor vec} log)
          ]
      (is (= (count all) (io/in! (app-log {:processor count}) log)))
      (is (= (first all) (io/in! (app-log {:processor first}) log)))
      (is (= (last all) (io/in! (app-log {:processor last}) log)))
      (is (= all all2))
      (is (= all all3))

      (let [{:keys [mark source app db user level code unk datetime timestamp]
             :as f}
            (first all)]
        (is (= 2 (-> f meta :head-line)))
        (is (= log (-> f meta :source)))
        (is (= "Sat May 05 13:00:27 2012" mark))
        (is (= "Local" source))
        (is (= "Sample" app))
        (is (= "" db))
        (is (= "" user))
        (is (= "Info" level))
        (is (= 1002035 code))
        (is (= "" unk))
        (is (instance? java.time.LocalDateTime datetime))
        (is (= "2012-05-05T13:00:27" timestamp)))

      (let [{:keys [mark source app db user level code unk datetime timestamp]
             :as l}
            (last all)]
        (is (= 4107 (-> l meta :head-line)))
        (is (= log (-> l meta :source)))
        (is (= "Wed Sep 05 00:11:09 2012" mark))
        (is (= "Local" source))
        (is (= "Sample" app))
        (is (= "" db))
        (is (= "" user))
        (is (= "Error" level))
        (is (= 1042030 code))
        (is (= "" unk))
        (is (instance? java.time.LocalDateTime datetime))
        (is (= "2012-09-05T00:11:09" timestamp))))))

(deftest Reading-Sample-Basic-log-from-datasets-test-with-unk

  (testing "Sample.Basic with UNK"

    (let [log "datasets/sample_basic_w_unk.log"
          all (io/in! (app-log {:processor vec}) log)]
      (is (= (count all) (io/in! (app-log {:processor count}) log)))
      (is (= (first all) (io/in! (app-log {:processor first}) log)))
      (is (= (last all) (io/in! (app-log {:processor last}) log)))

      (let [{:keys [mark source app db user level code unk datetime timestamp]
             :as f}
            (first all)]
        (is (= 2 (-> f meta :head-line)))
        (is (= log (-> f meta :source)))
        (is (= "Sat May 05 13:00:27 2012" mark))
        (is (= "Local" source))
        (is (= "Sample" app))
        (is (= "" db))
        (is (= "" user))
        (is (= "Info" level))
        (is (= 1002035 code))
        (is (= "2633" unk))
        (is (instance? java.time.LocalDateTime datetime))
        (is (= "2012-05-05T13:00:27" timestamp)))

      (let [{:keys [mark source app db user level code unk datetime timestamp]
             :as l}
            (last all)]
        (is (= 4107 (-> l meta :head-line)))
        (is (= log (-> l meta :source)))
        (is (= "Wed Sep 05 00:11:09 2012" mark))
        (is (= "Local" source))
        (is (= "Sample" app))
        (is (= "" db))
        (is (= "" user))
        (is (= "Error" level))
        (is (= 1042030 code))
        (is (= "35742" unk))
        (is (instance? java.time.LocalDateTime datetime))
        (is (= "2012-09-05T00:11:09" timestamp))))))

(deftest Reading-MaxL-Spool-log-from-datasets-test

  (testing "MaxL output"
    (let [spool "datasets/maxl-output.log"
          all   (io/in! (maxl-spool {:processor vec, :column-width 256}) spool)
          all2  ((maxl-spool {:processor vec, :column-width 256}) spool)
          all3  (maxl-spool {:processor vec, :column-width 256} spool)]
      (is (= (count all)
             (io/in! (maxl-spool {:processor count, :column-width 256}) spool)))
      (is (= (first all)
             (io/in! (maxl-spool {:processor first, :column-width 256}) spool)))
      (is (= (last all)
             (io/in! (maxl-spool {:processor last, :column-width 256}) spool)))
      (is (= all all2))
      (is (= all all3))

      (let [{:keys [head data tail] :as f} (first all)]
        (is (= 9 (count data)))
        (is (every? string? head))
        (is (every? map? data))
        (is (every? string? tail))
        (is (= spool (-> f meta :source)))
        (is (= 0 (-> f meta :head-line)))
        (is (= 2 (-> f meta :data-line)))
        (is (= 13 (-> f meta :tail-line))))

      (let [{:keys [head data tail]} (last all)]
        (is (= 4 (count data)))
        (is (every? string? head))
        (is (every? map? data))
        (is (every? string? tail))))))
