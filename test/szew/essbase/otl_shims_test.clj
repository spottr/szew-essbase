; Copyright (c) Sławek Gwizdowski
;
; Permission is hereby granted free of charge to any person obtaining
; a copy of this software and associated documentation files (the "Software")
; to deal in the Software without restriction including without limitation
; the rights to use copy modify merge publish distribute sublicense
; and/or sell copies of the Software and to permit persons to whom the
; Software is furnished to do so subject to the following conditions:
;
; The above copyright notice and this permission notice shall be included
; in all copies or substantial portions of the Software.
;
; THE SOFTWARE IS PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND EXPRESS
; OR IMPLIED INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY
; FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
; THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM DAMAGES OR OTHER
; LIABILITY WHETHER IN AN ACTION OF CONTRACT TORT OR OTHERWISE ARISING
; FROM OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
; IN THE SOFTWARE.
;
(ns ^{:author "Sławek Gwizdowski"
      :doc "Essbase XML Outline export - pre 0.2.0 tests."}
 szew.essbase.otl-shims-test
 (:require
  [szew.essbase.otl :as otl
   :refer [list-content-tags list-dimensions extract-dimensions member-seq
           member-walk member-set member-lut lut-package]]
  [szew.essbase.otl.member :refer [map->OutlineMember]]
  [szew.essbase.otl.dimension :refer [map->OutlineDimension]]
  [szew.io :as io]
  [clojure.test :refer [deftest testing is use-fixtures]]
  [clojure.zip :as zip]))


(deftest Dimension-parsing-testing

  (testing "Misc Helpers testing"
    (is (= [:Dimension :Dimension :Dimension :Dimension
            :Dimension :Dimension :Dimension :Dimension
            :Dimension :Dimension :cube :smartLists]
           (io/in! (io/xml {:processor list-content-tags})
                   "datasets/sample_basic.xml"))))

  (testing "Dimension listing"
    (let [dims (io/in! (io/xml {:processor (comp vec list-dimensions)})
                       "datasets/sample_basic.xml")]
      (is (= 10 (count dims)))
      (is (= ["Year" "Measures" "Product" "Market" "Scenario"
              "Caffeinated" "Ounces" "Pkg Type" "Population" "Intro Date"]
             (mapv :name dims)))))

  (testing "Dimension processing"
    (let [dims (io/in! (io/xml {:processor (comp vec extract-dimensions)})
                       "datasets/sample_basic.xml")
          names-counts (mapv (juxt :name :member-count) dims)]
      (is (= [["Year" 16]
              ["Measures" 16]
              ["Product" 21]
              ["Market" 24]
              ["Scenario" 4]
              ["Caffeinated" 2]
              ["Ounces" 4]
              ["Pkg Type" 2]
              ["Population" 14]
              ["Intro Date" 7]]
             names-counts))

      (let [year (first dims)
            qtr1 (-> dims first :members first)
            jan  (-> dims first :members first :members first)]
        (is (= (map->OutlineDimension
                {:attribute-nodes #{}
                 :attribute-type nil
                 :contained [:Generations :Member :Member :Member :Member]
                 :csversion "4.0"
                 :data-storage "DynamicCalc"
                 :density "Dense"
                 :dimension-type "Time"
                 :generations {1 "History", 2 "Quarter", 3 "Months"}
                 :hierarchy-type "Stored"
                 :is-compression-dimension false
                 :levels {}
                 :member-count 16
                 :misc {}
                 :name "Year"
                 :storage-category nil
                 :tag :Dimension})
               (assoc year :members nil)))
        (is (= (map->OutlineMember
                {:alias "Quarter1"
                 :aliases {"Long Names" "Quarter1"}
                 :attributes {}
                 :consolidation "+"
                 :contained [:Alias :Member :Member :Member]
                 :data-storage "DynamicCalc"
                 :dimension "Year"
                 :generation 1
                 :hierarchy-type "Disabled"
                 :level0? false
                 :member-formula nil
                 :members nil
                 :misc {}
                 :name "Qtr1"
                 :parent "Year"
                 :path ["Year"]
                 :shared-member false
                 :storage-category nil
                 :tag :Member
                 :time-balance nil
                 :two-pass-calc false
                 :uda #{}
                 :variance-reporting nil})
               (assoc qtr1 :members nil)))
        (is (= (map->OutlineMember
                {:alias "January"
                 :aliases {"Long Names" "January"}
                 :attributes {}
                 :consolidation "+"
                 :contained [:Alias]
                 :data-storage "StoreData"
                 :dimension "Year"
                 :generation 2
                 :hierarchy-type nil
                 :level0? true
                 :member-formula nil
                 :members nil
                 :misc {}
                 :name "Jan"
                 :parent "Qtr1"
                 :path ["Year" "Qtr1"]
                 :shared-member false
                 :storage-category nil
                 :tag :Member
                 :time-balance nil
                 :two-pass-calc false
                 :uda #{}
                 :variance-reporting nil})
               jan)))

      (let [measures (second dims)
            cogs (-> measures :members first
                     :members first
                     :members second)]
        (is (= (map->OutlineMember
                {:alias "Cost of Goods Sold"
                 :aliases {"Long Names" "Cost of Goods Sold"}
                 :attributes {}
                 :consolidation "-"
                 :contained [:Alias]
                 :data-storage "StoreData"
                 :dimension "Measures"
                 :generation 3
                 :hierarchy-type nil
                 :level0? true
                 :member-formula nil
                 :members nil
                 :misc {}
                 :name "COGS"
                 :parent "Margin"
                 :path ["Measures" "Profit" "Margin"]
                 :shared-member false
                 :storage-category nil
                 :tag :Member
                 :time-balance nil
                 :two-pass-calc false
                 :uda #{}
                 :variance-reporting "Expense"})
               cogs)))

      (let [product (nth dims 2)
            colas (-> product :members first)
            cola (-> product :members first :members first)]
        (is (= (map->OutlineDimension
                {:attribute-nodes #{"Caffeinated" "Intro Date" "Ounces" "Pkg Type"}
                 :attribute-type nil
                 :contained [:AttributeDimension
                             :AttributeDimension
                             :AttributeDimension
                             :AttributeDimension
                             :Generations
                             :Levels
                             :Member
                             :Member
                             :Member
                             :Member
                             :Member]
                 :csversion "4.0"
                 :data-storage "StoreData"
                 :density "Sparse"
                 :dimension-type "Standard"
                 :generations {2 "Category", 3 "Product SKU"}
                 :hierarchy-type "Stored"
                 :is-compression-dimension false
                 :levels {0 "SKU", 1 "Family"}
                 :members nil
                 :member-count 21
                 :misc {}
                 :name "Product"
                 :storage-category nil
                 :tag :Dimension})
               (assoc product :members nil)))
        (is (= (map->OutlineMember
                {:alias "Colas"
                 :aliases {"Default" "Colas"}
                 :attributes {}
                 :consolidation "+"
                 :contained [:Alias :Member :Member :Member]
                 :data-storage "StoreData"
                 :dimension "Product"
                 :generation 1
                 :hierarchy-type "Disabled"
                 :level0? false
                 :member-formula nil
                 :members nil
                 :misc {}
                 :name "100"
                 :parent "Product"
                 :path ["Product"]
                 :shared-member false
                 :storage-category nil
                 :tag :Member
                 :time-balance nil
                 :two-pass-calc false
                 :uda #{}
                 :variance-reporting nil})
               (assoc colas :members nil)))
        (is (= (map->OutlineMember
                {:alias "Cola"
                 :aliases {"Default" "Cola"}
                 :attributes {"Caffeinated" "True"
                              "Intro Date" "03-25-1996"
                              "Ounces" "12"
                              "Pkg Type" "Can"}
                 :consolidation "+"
                 :contained [:AttributeMember
                             :AttributeMember
                             :AttributeMember
                             :AttributeMember
                             :Alias]
                 :data-storage "StoreData"
                 :dimension "Product"
                 :generation 2
                 :hierarchy-type nil
                 :level0? true
                 :member-formula nil
                 :members nil
                 :misc {}
                 :name "100-10"
                 :parent "100"
                 :path ["Product" "100"]
                 :shared-member false
                 :storage-category nil
                 :tag :Member
                 :time-balance nil
                 :two-pass-calc false
                 :uda #{}
                 :variance-reporting nil})
               cola)))

      (let [pkg-type (nth dims 7)
            can (-> pkg-type :members last)]
        (is (= (map->OutlineDimension
                {:attribute-nodes #{}
                 :attribute-type "Text"
                 :contained [:Member :Member]
                 :csversion "4.0"
                 :data-storage "DynamicCalc"
                 :density "Sparse"
                 :dimension-type "Attribute"
                 :generations {}
                 :hierarchy-type "Stored"
                 :is-compression-dimension false
                 :levels {}
                 :members nil
                 :member-count 2
                 :name "Pkg Type"
                 :misc {}
                 :storage-category "Attribute"
                 :tag :Dimension})
               (assoc pkg-type :members nil)))
        (is (= (map->OutlineMember
                {:alias ""
                 :aliases {}
                 :attributes {}
                 :consolidation "~"
                 :contained []
                 :data-storage "DynamicCalc"
                 :dimension "Pkg Type"
                 :generation 1
                 :hierarchy-type "Disabled"
                 :level0? true
                 :member-formula nil
                 :members nil
                 :misc {}
                 :name "Can"
                 :parent "Pkg Type"
                 :path ["Pkg Type"]
                 :shared-member false
                 :storage-category "Attribute"
                 :tag :Member
                 :time-balance nil
                 :two-pass-calc false
                 :uda #{}
                 :variance-reporting nil})
               can)))))

  (testing "Filtering members"
    (let [wanted (comp (partial contains? #{"Jan" "Jun" "Dec"}) :name)
          selector (comp (partial filterv wanted)
                         member-seq
                         first
                         (partial extract-dimensions
                                  (comp (partial = "Year") :name)))
          result (io/in! (io/xml {:processor selector})
                         "datasets/sample_basic.xml")]
      (is (= #{"Jan" "Jun" "Dec"} (into (hash-set) (map :name) result)))))

  (testing "Visiting members"
    (let [wanted (comp (partial contains? #{"Jan" "Jun" "Dec"}) :name)
          mark (fn [loc]
                 (if (wanted (zip/node loc))
                   (-> loc (zip/edit assoc :X true)) loc))
          selector (comp (partial filterv wanted)
                         member-seq
                         (partial member-walk mark)
                         first
                         (partial extract-dimensions
                                  (comp (partial = "Year") :name)))
          result (io/in! (io/xml {:processor selector})
                         "datasets/sample_basic.xml")]
      (is (= #{"Jan" "Jun" "Dec"} (into (hash-set) (map :name) result)))))

  (testing "Member set"
    (let [dims (io/in! (io/xml {:processor (comp vec extract-dimensions)})
                       "datasets/sample_basic.xml")
          counts (mapv (juxt :name (comp count member-set)) dims)]
      (is (= [["Year" 17]
              ["Measures" 17]
              ["Product" 19] ;; 3 shared members
              ["Market" 25]
              ["Scenario" 5]
              ["Caffeinated" 3]
              ["Ounces" 5]
              ["Pkg Type" 3]
              ["Population" 15]
              ["Intro Date" 8]]
             counts))))

  (testing "Member LUT"
    (let [proc (comp (partial into (hash-set))
                     (partial map :name)
                     (partial mapcat (partial drop 1))
                     (partial map member-seq)
                     extract-dimensions)
          uniq (io/in! (io/xml {:processor proc})
                       "datasets/sample_basic.xml")
          lut (io/in! (io/xml {:processor member-lut})
                      "datasets/sample_basic.xml")
          pkg (lut-package "datasets/sample_basic.xml")]
      ;; 110 total, 3 shared, 107 distinct names
      (is (= 107 (count uniq)))
      ;(is (every? string? (map lut uniq)))
      (is (nil? (lut "Non-existent Member Name!")))
      ;; BREAKING change in the LUT API:
      (is (not= :not-found (lut :not-found "Non-existent Member Name!")))
      (is (= (-> lut meta :lut)
             (-> pkg :m->d meta :lut)))
      (is (= 5 (:dim-count pkg))))))
