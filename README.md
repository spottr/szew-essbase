# szew/essbase

Consume various files produced by Essbase.

[![szew/essbase](https://clojars.org/szew/essbase/latest-version.svg)](https://clojars.org/szew/essbase)

[API documentation][latest] | [Changelog][changelog]

_Please note: API breakage will happen in minor versions (0.X.0) before 1.0.0
is released. Only patch versions (0.n.X) are safe until then._

** Table of Contents **

[TOC]

---

## Why

I've been dogfooding my private Clo*j*ure toolbox (named `szew`) since 2012.

Splitting out and releasing non-proprietary parts.

### What's Here

Namespace breakdown:

* `szew.essbase.aso`  -- native ASO data exports
* `szew.essbase.bso`  -- native BSO data exports
* `szew.essbase.cols` -- native column exports
* `szew.essbase.logs` -- Application logs and MaxL spools
* `szew.essbase.otl`  -- native XML outline exports
* `szew.essbase.txl`  -- BSO transaction logs (alg+atx)

Processing is based on contracts established by `szew.io`, namely `szew.io/in!`
protocol method, but most of the API should hide that fact until you need
to go deeper than `predicate?` + data file.

## Usage

Like all the other `szew` pieces this library is REPL workflow oriented and
with a bit of scripting in mind. Please forgive scoped `def`s below, these
are here for presentation purposes only. Consider functions as calculation
stages and defined variables as demonstration values to analyze.

```clojure
(ns dev
 (:require
   [szew.io :as io]
   [szew.essbase.aso :as ess.aso]
   [szew.essbase.bso :as ess.bso]
   [szew.essbase.cols :as ess.cols]
   [szew.essbase.logs :as ess.logs]
   [szew.essbase.txl :as ess.txl]
   [szew.essbase.otl :as ess.otl]))

;; Process the outlines (XML exports)

(defn outlines!
 ([fname]
  (def otl fname)

  ;; XML -> dimension roots, sans members
  (def dimension-list (ess.otl/list-dimensions! otl))

  ;; XML -> full dimensions
  (def dimensions (ess.otl/extract-dimensions! otl))

  ;; XML -> cube data structure
  (def cube (ess.otl/cube! otl))

  ;; XML to member -> dimension lookup table (hash-map)
  (def lut (ess.otl/member-lut! otl))

  ;; XML to {:m->d {...} :dimensions [...] :dim-count N}
  (def lut-pkg (ess.otl/member-lut-package! otl))
  :done)
 ([]
  (outlines! "datasets/sample_basic.xml")))

;; library expecation check for dimensions
(defn borked-dims []
  (letfn [(de-member [m] (assoc m :members []))
          (pick? [m] (not (s/valid? ::dim/dimension m)))]
    (filter pick? (map de-member dimensions))))

;; library expectation check for member
(defn borked-members []
  (letfn [(de-member [m] (assoc m :members []))
          (pick? [m] (not (s/valid? ::mbr/member m)))]
    (->> dimensions
      (mapcat (comp (partial drop 1) ess.otl/member-seq))
      (map de-member)
      (filter pick?))))

;; Process native BSO exports

;; Requires member->dimension lookup and number of dimensions, we need lut-pkg
(defn block-storage! []
  (when-not (bound? #'lut-pkg)
    (outlines!))
  (let [bso  "datasets/sample_basic_level0.txt"
        tnc  (fn [[coords value]]
              (and (= "Texas" (coords "Market"))
                   (= "COGS" (coords "Measures"))))
        ;; attach :processor to lut-pkg and you're ready to go!
        pick (ess.bso/cells (assoc lut-pkg :processor (partial filterv tnc)))]
    (def texas-cogs (pick bso))
    :done))

;; Transaction Logs processing aka TxL
;; Filtering, matching, drift discovery and correction.

(defn trails! []
  (let [p (comp ess.txl/printable
                (partial filterv (ess.txl/user-matches? #"johny")))
        alg "datasets/sample.alg"
        atx "datasets/sample.atx"
        btx "datasets/sample_borked.atx"]

    (def txl    (io/in! (ess.txl/tx-log {:processor p}) {:alg alg :atx atx}))
    (def txl1   ((ess.txl/tx-log {:processor first}) {:alg alg :atx atx}))
    (def txl2   ((ess.txl/tx-log {:processor second}) {:alg alg :atx atx}))
    (def txlc   ((ess.txl/tx-log {:processor count}) {:alg alg :atx atx}))
    (def drifts ((ess.txl/tx-log {:processor ess.txl/drifts})
                 {:alg alg :atx btx}))
    (def alg-c  ((ess.txl/tx-log {:processor ess.txl/correct-drifts})
                 {:alg alg :atx atx}))
    (def alg-f  ((ess.txl/tx-log {:processor ess.txl/correct-drifts})
                 {:alg alg :atx btx}))
    :done))

;; MaxL spool: consume MaxL output, need to just know (set) column width right.

(defn spools! []
  (def spools (io/in! (ess.logs/maxl-spool {:processor vec, :column-width 256})
                      "datasets/maxl-output.log"))
  :done)
```

## Known Limitations

This library is built on things I encountered in the wild while working
with Hyperion 11.1 version. It recently came to my attention that some
changes were made to the XML format in 11.2, where new content attributes
were added and member formulas were moved from attribute to content. This
library tries to be lenient and accepting, but if something is missing
first step should be to get an export from `Sample`.`Basic` and compare
outputs.

### No "Duplicate Member" Support (Yet)

I've never worked with outlines that had "duplicate members" enabled, therefore
handling of these was never implemented. Member LUT and data processing assumes
that all Member names are Dimension unique -- having shared members is fine,
having the same name mean different things across dimensions will be trouble.

If anyone is interested in having this -- hit me up with outline and data
samples, I'll take a crack at it.

### Illegal Reflection Warning In XML Processing

Unfortunately `data.xml` causes JDK9+ to have a fit about some reflections.
Clojure FAQ [Why do I get an illegal access warning?][FAQia] entry uses
the very call as example. Adding `--illegal-access=deny` is recommended until
this is fixed in upstream.

### Breaking Changes

Until version 1.0.0 is reached minor version upgrades may introduce
incompatible changes and deprecations. These will be clearly listed in
[changelog][changelog] and reflected in the [API documentation][latest].

* Breaking changes in `szew.essbase.txl` in 0.3.0.
* Multiple things broken and deprecated in 0.2.0.

## License

Copyright © 2012-2021 Sławek Gwizdowski

**MIT License, text can be found in the LICENSE file.**

[latest]: http://spottr.bitbucket.io/szew-essbase/latest/
[changelog]: CHANGELOG.md
[FAQia]: https://clojure.org/guides/faq#illegal_access
