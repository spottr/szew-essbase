# Change Log

All notable changes to this project will be documented in this file. This change log follows the conventions of [keepachangelog.com](https://keepachangelog.com/).

## Wishlist

### Changes

- Records for Cell and Record for performance/memory.
- Something smarter than {Dimension Member} coordinates for Cells?
- Something smarter than {Member Dimension} m->d memory-use-wise?

## [Unreleased]


## [0.3.4] - 2021-09-06

### Fixes

- Member formula has moved from attr to content in 11.2, so now we first
  try attribute and then content.

## [0.3.3] - 2021-09-04

### Changes

- Ongoing documentation touch-ups.
- `OutlineMember` and `OutlineDimension` record types.
- Improved `szew.essbase.outline/member-seq`.
- Deprecations in `szew.essbase.logs`: `maxl-prompt?` and `maxl-constants`.
- Added input timestamp format and locale in `szew.essbase.logs/AppLog`.
- Switched from `clj-time` to `clojure.java-time`.
- Changed coding style a bit to respect vertical space more.

## [0.3.2] - 2020-03-31

### Changes

- Linted source with `clj-kondo` and addressed all reported issues.
- Removed shared state from tests, local state instead.
- Added version metadata to functions.

### Fixes

- Eliminated reflection warning within `MaxLSpool`s `in!` method.
- Subbed some `(into (hash-set) ...)` calls with `set`.
- Added missing `szew.essbase.otl/zipper` shim from 0.2.0 and deprecated it.

### Added

- Separate `szew.essbase.cols/sniff-unknown` implementation.
- `([spec source])` arity to constructors `szew.io` alike to `szew.essbase`:
  `aso/members`, `aso/cells`, `bso/members`, `bso/records`, `bso/cells`,
  `cols/members`, `cols/records`, `cols/cells`, `logs/app-log`,
  `logs/maxl-spool`, `txl/tx-log` and tests.

## [0.3.1] - 2019-08-23

### Added

- `txl.printable` now has a transducer arity. And tests.

## [0.3.0] - 2019-07-20

### Fixes

- BREAKING: `txl`: fix :index to 1-based, that's how people count, sorry.
- BREAKING: `txl`: fix :at to 1-based, that's how people count, sorry again.
- BREAKING: `txl`: empty line now included in blocks.
### Changes

- BREAKING: `txl.drifts` return map shape changed, see doc.
- BREAKING: `txl.drifts` now list differences (ALG-ATX) from :ctrl header.
- BREAKING: `txl.drifts` only misaligned entries are flagged now.
### Added

- `txl` :line-no in header and :span in data added, physical lines, 1-based.
- `txl.list-drifts` lazily returns entries with issues.
- `txl.correct-drifts` generates fixed ALG entries based on :ctrl headers.
- `txl.correct-alg-drifts!` writes a complete fixed ALG to file (w/ header).

## [0.2.0] - 2019-06-18

### Changes

- Convention: functions working on files are marked with `!`.
- Rewrite of `szew.essbase.otl`, API rework some breaking changes.
- DEPRECATED: `otl/member`, use `seed-member`.
- DEPRECATED: `otl/expanded-member`, use `expand-member`.
- DEPRECATED: `otl/dimension`, use `seed-dimension`.
- DEPRECATED: `otl/expanded-dimension`, use `expand-dimension`.
- DEPRECATED: `otl/member-set`, use `member-name-set`.
- DEPRECATED: `otl/lut-fn`, use `prepare-lut`.
- DEPRECATED: `otl/lut-package`, use `member-lut-package!`.
- BREAKING: member LUT is now a hash-map, so argument order is swapped, from:
```clojure

  ;; prior
  (lut :default "member name")

  ;; current
  (lut "member name" :default)
```

### Added

- Contracts and specs validation via `clojure.spec.alpha`.
- Compatibility shims in `otl` where possible and `otl-shims-test`.
- `otl/list-dimensions!` to pull directly from XML outline export.
- `otl/extract-dimensions!` to pull directly from XML outline export.
- <cube> processing: `otl/read-cube`, `otl/cube`, `otl/cube!`.
- `Members`, `Records` and `Cells` can be called directly now (ASO, BSO, cols).

## [0.1.1] - 2017-07-31

### Fixes

- Fixed szew.essbase.logs/app-log to accept unknown numeric value in header.

## [0.1.0] - 2017-06-01

### Initial Release

- This is the first extraction from private `szew` toolkit.
  * `szew.essbase.aso`  -- ASO data export
  * `szew.essbase.bso`  -- BSO data export
  * `szew.essbase.cols` -- Column export
  * `szew.essbase.logs` -- Application logs and MaxL spools
  * `szew.essbase.otl`  -- Outline XML export processing
  * `szew.essbase.txl`  -- BSO transaction logs (alg+atx)


[Unreleased]: https://bitbucket.org/spottr/szew-essbase/branches/compare/master%0D0.3.4#diff
[0.3.4]: https://bitbucket.org/spottr/szew-essbase/branches/compare/0.3.4%0D0.3.3#diff
[0.3.3]: https://bitbucket.org/spottr/szew-essbase/branches/compare/0.3.3%0D0.3.2#diff
[0.3.2]: https://bitbucket.org/spottr/szew-essbase/branches/compare/0.3.2%0D0.3.1#diff
[0.3.1]: https://bitbucket.org/spottr/szew-essbase/branches/compare/0.3.1%0D0.3.0#diff
[0.3.0]: https://bitbucket.org/spottr/szew-essbase/branches/compare/0.3.0%0D0.2.0#diff
[0.2.0]: https://bitbucket.org/spottr/szew-essbase/branches/compare/0.2.0%0D0.1.1#diff
[0.1.1]: https://bitbucket.org/spottr/szew-essbase/branches/compare/0.1.1%0D0.1.0#diff
[0.1.0]: https://bitbucket.org/spottr/szew-essbase/branches/compare/0.1.0%0D9d31ee363f9ca733533acd879b5aa2415ecf3b34#diff
