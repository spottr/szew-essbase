; Copyright (c) Sławek Gwizdowski
;
; Permission is hereby granted, free of charge, to any person obtaining
; a copy of this software and associated documentation files (the "Software"),
; to deal in the Software without restriction, including without limitation
; the rights to use, copy, modify, merge, publish, distribute, sublicense,
; and/or sell copies of the Software, and to permit persons to whom the
; Software is furnished to do so, subject to the following conditions:
;
; The above copyright notice and this permission notice shall be included
; in all copies or substantial portions of the Software.
;
; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
; OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
; FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
; THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
; LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
; FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
; IN THE SOFTWARE.
;
(ns ^{:author "Sławek Gwizdowski"
      :doc "Essbase BSO export.

What to expect from input:

* Space separated.
* Quoted member names, non-quoted values.
* Max file size is 2GB.
* COLUMNS are specified in first line of the file:
  - List of quoted member names of single dense dimension, last field is empty.
  - No members from this dimension ever appear in the file again.
  - N members are specified, up to these many figures can appear in data lines.
* POV lines appear periodically, those signal complete POV update:
  - List of quoted member names from distinct sparse dimensions.
* DATA line consists of both partial POV updates and figures:
  - Quoted members of remaining dense dimensions (not present in POV lines).
  - Figures are non-quoted #Mi and numeric values, up to N occurrences per line.
  - Last field is always empty string, so last figure is followed by space.
  - Missing values from the left are marked as #Mi, from the right - skipped.

To parse the export file you need to know two things:

* Number of data storing dimensions in the cube.
* Complete mapping of members to dimensions for storage dimensions."}
 szew.essbase.bso
 (:require
   [szew.io :as io]
   [clojure.java.io :as clj.io :refer [reader]]
   [clojure-csv.core :as csv]
   [clojure.spec.alpha :as s])
 (:import
   [java.io BufferedReader]
   [clojure.lang IFn]))

(defn data-field?
  "Take field, see if it's a possible Essbase value (empty, #Mi or number)."
  {:added  "0.1.0"
   :static true}
  [field]
  (or (= "" field)
      (= "#Mi" field)
      (try
        (double? (Double/parseDouble field))
        (catch NumberFormatException _ false))))

(defn data-line?
  "Check if last field is empty and penultimate field is a data-field."
  {:added  "0.1.0"
   :static true}
  [row]
  (and (= "" (last row)) (data-field? (last (pop row)))))

(s/def ::dim-count (s/and int? pos?))

(s/def
  ::members-args
  (s/or :none (s/nilable (s/and map? empty?))
        :some (s/keys :opt-un [:io/processor ::dim-count :io/encoding])))

(s/def
  ::members
  (s/keys :req-un [:io/processor ::dim-count :io/encoding]))

(defrecord Members [processor dim-count encoding]
  io/Input
  (io/in! [spec source]
    (when-not (s/valid? ::members spec)
      (throw
        (ex-info "Members spec validation failed!"
                 {:explanation (s/explain-data ::members spec)
                  :source      source})))
    (letfn [(rip [block]
              (let [pov  (ffirst block)
                    data (second block)
                    nip  (- (dec dim-count) (count pov))
                    nipr (comp vec (partial take nip))]
                (into (vec pov) (mapcat nipr data))))
            (splice [rows]
              (let [columns (-> rows first vec pop)
                    blocks  (->> rows
                                 rest
                                 (partition-by data-line?)
                                 (partition 2 2 nil))]
                (concat columns (mapcat rip blocks))))]
      (io! "Reading files here!"
           (with-open [^BufferedReader r (reader source :encoding encoding)]
             (processor (splice (csv/parse-csv r :delimiter \space)))))))
  IFn
  (applyTo [spec args]
    (when-not (= (count args) 1)
      (throw (ex-info "Wrong number of arguments! Expected 1."
                      {:spec spec
                       :args args})))
    (io/in! spec (first args)))
  (invoke [spec source]
    (io/in! spec source)))

(defn members
  "Processor gets member name per every occurrence in file.

  Default processor creates a hash-set of member names.

  It will discard all the figures, provided dim-count is correct, and only
  process members."
  {:added  "0.1.0"
   :static true}
  ([]
   (Members. set 0 "UTF-8"))
  ([spec]
   (into (members) spec))
  ([spec source]
   (io/in! (members spec) source)))

(s/fdef
  members
  :args (s/alt :0-args (s/cat)
               :1-arg  (s/cat :spec ::members-args)
               :2-args (s/cat :spec ::members-args :source any?))
  :ret  (s/or :members (s/and (partial instance? Members)
                              (s/keys :req-un [:io/processor :io/encoding]))
              :other   any?))

(s/def
  ::records-args
  (s/or :none (s/nilable (s/and map? empty?))
        :some (s/keys :opt-un [:io/processor ::dim-count :io/encoding])))

(s/def
  ::records
  (s/keys :req-un [:io/processor :otl/m->d ::dim-count :io/encoding]))

(defrecord Records [processor m->d dim-count encoding]
  io/Input
  (io/in! [spec source]
    (when-not (s/valid? ::records spec)
      (throw
        (ex-info "Records spec validation failed!"
                 {:explanation (s/explain-data ::records spec)
                  :source      source})))
    (letfn [(ok? [record]
              (if (contains? (first record) nil)
                (-> (format "Unknown dimension of: %s" (get (first record) nil))
                    (ex-info {:record    record
                              :pov-line  (-> record meta :pov-line)
                              :data-line (-> record meta :data-line)})
                    (throw))
                record))
            (rip [columns block]
              (let [pov-line (first (ffirst block))
                    pov      (second (ffirst block))
                    datas    (second block)
                    nip      (- (dec dim-count) (count pov))
                    nipr     (comp vec (partial take nip))
                    tuckr    (comp vec (partial drop nip))]
                (for [data datas]
                  (with-meta [(->> (concat pov (nipr (second data)))
                                   (mapv (juxt m->d identity))
                                   (into (hash-map)))
                              (->> (butlast (tuckr (second data)))
                                   (mapv vector columns)
                                   (into (hash-map)))]
                             {:pov-line  pov-line
                              :data-line (first data)
                              :source    source
                              :pov       pov
                              :data      (second data)}))))
            (splice [rows]
              (let [columns (-> rows first vec pop)
                    blocks  (->> rows
                                 (map vector (range))
                                 rest
                                 (partition-by (comp data-line? second))
                                 (partition 2 2 nil))]
                (map ok? (mapcat rip (repeat columns) blocks))))]
      (io! "Reading files here!"
        (with-open [^BufferedReader r (reader source :encoding encoding)]
          (processor (splice (csv/parse-csv r :delimiter \space)))))))
  IFn
  (applyTo [spec args]
    (when-not (= (count args) 1)
      (throw (ex-info "Wrong number of arguments! Expected 1."
                      {:spec spec
                       :args args})))
    (io/in! spec (first args)))
  (invoke [spec source]
    (io/in! spec source)))

(defn records
  "Processor will get data point per each data-line in file.

  Requires complete member to dimension mapping in m->d and correct data storing
  dimension count in dim-count.

  Processor gets seq of [{dimension member} {column value}].

  Default processor will return a vector of such data points.

  Errors out with ex-info if any member is mapped to nil."
  {:added  "0.1.0"
   :static true}
  ([]
   (Records. vec {} 0 "UTF-8"))
  ([spec]
   (into (records) spec))
  ([spec source]
   (io/in! (records spec) source)))

(s/fdef
  records
  :args (s/alt :0-args (s/cat)
               :1-arg  (s/cat :spec ::records-args)
               :2-args (s/cat :spec ::records-args :source any?))
  :ret  (s/or :records (s/and (partial instance? Records)
                              (s/keys :opt-un [:io/processor :io/encoding]))
              :other   any?))

(s/def
  ::cells-args
  (s/or :none (s/nilable (s/and map? empty?))
        :some (s/keys :opt-un [:io/processor :otl/m->d ::dim-count
                               :io/encoding])))

(s/def
  ::cells
  (s/keys :req-un [:io/processor :otl/m->d ::dim-count :io/encoding]))

(defrecord Cells [processor m->d dim-count encoding]
  io/Input
  (io/in! [spec source]
    (when-not (s/valid? ::cells spec)
      (throw
        (ex-info "Cells spec validation failed!"
                 {:explanation (s/explain-data ::cells spec)
                  :source      source})))
    (letfn [(split [record]
              (for [[column value] (second record)]
                (with-meta [(assoc (first record) (m->d column) column) value]
                  (meta record))))
            (splitter [records]
              (processor (mapcat split records)))]
      (io/in! (records (assoc spec :processor splitter)) source)))
  IFn
  (applyTo [spec args]
    (when-not (= (count args) 1)
      (throw (ex-info "Wrong number of arguments! Expected 1."
                      {:spec spec
                       :args args})))
    (io/in! spec (first args)))
  (invoke [spec source]
    (io/in! spec source)))

(defn cells
  "Processor will get data point per each cell in file.

  Requires complete member to dimension mapping in m->d and correct data
  dimension count in dim-count.

  Processor gets seq of [{dimension member} value].

  Default processor will return a vector of cells.

  Errors out with ex-info if any member is mapped to nil."
  {:added  "0.1.0"
   :static true}
  ([]
   (Cells. vec {} 0 "UTF-8"))
  ([spec]
   (into (cells) spec))
  ([spec source]
   (io/in! (cells spec) source)))

(s/fdef
  cells
  :args (s/alt :0-args (s/cat)
               :1-arg  (s/cat :spec ::cells-args)
               :2-args (s/cat :spec ::cells-args :source any?))
  :ret   (s/or :cells (s/and (partial instance? Cells)
                             (s/keys :req-un [:io/processor :io/encoding]))
               :other any?))

;; Helper functions -- Members

(defn sniff-unknown
  "Creates a processor for Members that will return a seq of distinct missing
  members."
  {:added  "0.1.0"
   :static true}
  [m->d]
  (fn distinct-unknown [member]
    (distinct (filterv (comp nil? m->d) member))))

;; Helper functions -- Cells

(defn sniff-dimensions
  "A processor for Cells that will return dimensions of first cell."
  {:added  "0.1.0"
   :static true}
  [cells]
  (set (keys (ffirst cells))))

;; Convert dump files into single value column

(defn dump->tsv
  "Given dump files consolidates them into single, row-expanded TSV.

  One row per cell.

  Requires complete member to dimension mapping in m->d and complete list of
  dimension names in order. It will be used to get dim-count right."
  {:added  "0.1.0"
   :static true}
  [m->d order out-path in-path & in-paths]
  (let [->line (fn [cell] (conj (mapv (first cell) order) (last cell)))
        sink   (io/sink (io/tsv) out-path)
        suck   (cells {:dim-count (count order)
                       :processor (comp sink (partial map ->line))
                       :m->d m->d})]
    (io/in! suck in-path)
    (when (seq in-paths)
      (let [sink+ (io/sink (io/tsv {:append true}) out-path)
            suck+ (cells {:dim-count (count order)
                          :processor (comp sink+ (partial map ->line))
                          :m->d m->d})]
        (doseq [in-path in-paths]
          (io/in! suck+ in-path))))))
