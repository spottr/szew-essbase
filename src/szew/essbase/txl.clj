; Copyright (c) Sławek Gwizdowski
;
; Permission is hereby granted, free of charge, to any person obtaining
; a copy of this software and associated documentation files (the "Software"),
; to deal in the Software without restriction, including without limitation
; the rights to use, copy, modify, merge, publish, distribute, sublicense,
; and/or sell copies of the Software, and to permit persons to whom the
; Software is furnished to do so, subject to the following conditions:
;
; The above copyright notice and this permission notice shall be included
; in all copies or substantial portions of the Software.
;
; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
; OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
; FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
; THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
; LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
; FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
; IN THE SOFTWARE.
;
(ns ^{:author "Sławek Gwizdowski"
      :doc "Essbase transaction logs.

ALG file is just pairs of timestamps and transaction descriptions:

* First two lines is the time stamp of when audit log was enabled.
* Remaining pairs describe user and location+length in ATX file, line wise.

ATX file holds the data as it was locked and sent:

* Quoted member names, non-quoted values.
* Data chunks are separated by empty lines.

Note: Entries in both files are expected to keep the same order. Based on these
properties they can be parsed separately and joined via index. This method
is beneficial, because row-count errors happen in ALG from time to time
and are easier to spot if you don't follow ALG declarations when parsing
transactions.

This namespace lets you process transaction logs, filter and pack results
in a presentable way. Contains some basic predicates to aid that.

IMPORTANT:

Since you will most likely work with these files via text editor, or interact
with people who will, to verify the results -- all indexing is 1-based, just
to make your life a bit easier easier:

* :line-no in header, physical line in ALG file;
* :span in block, physical lines in ATX file;
* :index in both is order number of it.

Whatever is the ALG declaration of starting position and rows -- we use that
here to avoid any confusion while exchanging this data."}
 szew.essbase.txl
 (:require
  [clojure.string :as string :refer [replace] :rename {replace re-replace}]
  [clojure.java.io :as clj.io :refer [reader]]
  [java-time :as jt]
  [szew.io :as io]
  [clojure-csv.core :as csv])
 (:import
  [java.util Locale]
  [java.time.format DateTimeFormatter]
  [java.io BufferedReader]
  [clojure.lang IFn]))

(def ^{:doc   "Input format for ALG timestamp."
       :added "0.1.0"}
  header-time-date-in
  (DateTimeFormatter/ofPattern "'['EEE MMM dd HH:mm:ss yyyy']'" Locale/US))

(def ^{:doc   "Output format for ALG timestamp."
       :added "0.1.0"}
  header-time-date-out
  (.withLocale DateTimeFormatter/ISO_DATE_TIME Locale/US))

(def ^{:doc   "Regular expression for ALG entries."
       :added "0.1.0"}
  header-payload-re #"(?x)Log\ Updates\ From\ User\ \[\ (.+)\ \]\s
                          Starting\ At\ Row\ \[\ (\d+)\ \]\s
                          For\ A\ Total\ Of\ \[\ (\d+)\ \]\ Rows")

(defn ->header
  "Consumes header index and lines, returns a header."
  {:added  "0.1.0"
   :static true}
  [idx two-line]
  (try
    (let [[u i d] (rest (re-find header-payload-re (second two-line)))
          ts      (jt/local-date-time header-time-date-in (first two-line))]
      {:timestamp ts
       :date      (jt/format header-time-date-out ts)
       :user      u
       :at        (Integer/parseInt i) ;; alg physical lines, starts from 1
       :rows      (Integer/parseInt d) ;; empty row is counted here
       :raw       two-line
       :index     (inc idx)
       :line-no   (+ 3 (* 2 idx))})
    (catch Exception ex
      (throw (ex-info "Oops in ->header."
                      {:index idx :lines two-line :line-no (+ 3 (* 2 idx))}
                      ex)))))

(defn ->block
  "Consumes block index & indexed lines, returns a block."
  {:added  "0.1.0"
   :static true}
  [idx lines]
  (let [lines* (conj (mapv (juxt (comp inc first) second) lines)
                     [(-> lines last first (+ 2)) ""])]
    {:block lines*
     :index (inc idx)
     :rows  (count lines*)
     :at    (ffirst lines*)
     :span  [(ffirst lines*) (+ (ffirst lines) (count lines*))]}))

(defn headers
  "Consume line-seq, returns sequence of headers (via ->header)."
  {:added  "0.1.0"
   :static true}
  [a-line-seq]
  (sequence (comp (drop 1) (map-indexed ->header))
            (partition 2 2 "" a-line-seq)))

(defn blocks
  "Consumes line-seq, returns sequence of blocks (via ->block)."
  {:added  "0.1.0"
   :static true}
  [a-line-seq]
  (sequence (comp (map-indexed vector)
                  (partition-by (comp (partial not= "") second))
                  (filter (comp (partial not= "") second first))
                  (map-indexed ->block))
            a-line-seq))

(defn header+block
  "Consumes header and block, merges the structure and adds control checks."
  {:added  "0.1.0"
   :static true}
  [header block]
  {:head header
   :data block
   :ctrl {:at-ok?     (= (:at header) (:at block))
          :at-drift   (- (:at header) (:at block))
          :rows-ok?   (= (:rows header) (:rows block))
          :rows-drift (- (:rows header) (:rows block))}})

(defn strict-header+block
  "Executes header+block, then throws exception if any checks failed."
  {:added  "0.1.0"
   :static true}
  [header block]
  (let [h+b (header+block header block)]
    (when-not (get-in h+b [:ctrl :rows-ok?] false)
      (throw (ex-info "Rows =/= block size" {:header+block h+b})))
    (when-not (get-in h+b [:ctrl :at-ok?] false)
      (throw (ex-info "Index =/= block start" {:header+block h+b})))
    h+b))

(defrecord TxLog [strict processor]
  io/Input
  (io/in! [_ {:keys [alg atx]}]
    (with-open [^BufferedReader l (reader alg :encoding "UTF-8")
                ^BufferedReader t (reader atx :encoding "UTF-8")]
      (if strict
        (processor (map strict-header+block
                        (headers (line-seq l))
                        (blocks (line-seq t))))
        (processor (map header+block
                        (headers (line-seq l))
                        (blocks (line-seq t)))))))
  IFn
  (applyTo [spec args]
    (when-not (= (count args) 1)
      (throw (ex-info "Wrong number of arguments! Expected 1."
                      {:spec spec
                       :args args})))
    (io/in! spec (first args)))
  (invoke [spec source]
    (io/in! spec source)))

(defn tx-log
  "Will create an instance of TxLog. This instance defined io/in! call:

  (io/in! tx-instance {:alg path-to-alg :atx path-to-atx})"
  {:added  "0.1.0"
   :static true}
  ([]
   (TxLog. false vec))
  ([spec]
   (into (tx-log) spec))
  ([spec source]
   (io/in! (tx-log spec) source)))

;; ## some helpers

(defn drifts
  "TxLog reducing processor: at and rows issues in ALG/ATX as a map.

  Only entries with issues are listed, relevant sub-maps are of shape:

    {index (- alg-value atx-value)}

  Fields:

  :at-drift provides start position mismatch between ALG and ATX.

  :rows-drift provides length mismatch between ALG and ATX.

  :processed is the pair of first and last index of processed h+b."
  {:added  "0.1.0"
   :static true}
  [entries]
  (let [at-hits   (volatile! [])   ;; HO HO HO... HO.
        rows-hits (volatile! [])
        start-idx (volatile! nil)
        final-idx (volatile! nil)]
    (loop [todo entries]
      (if-let [curr (first todo)]
        (let [{:keys [at-ok? rows-ok? at-drift rows-drift]} (:ctrl curr)
              index  (get-in curr [:head :index])]
          (when (nil? @start-idx)
            (vreset! start-idx index))
          (vreset! final-idx index)
          (when-not at-ok?
            (vswap! at-hits conj [index at-drift]))
          (when-not rows-ok?
            (vswap! rows-hits conj [index rows-drift]))
          (recur (rest todo)))
        {:at-drift   (into (sorted-map) @at-hits)
         :rows-drift (into (sorted-map) @rows-hits)
         :processed  [@start-idx @final-idx]}))))

(defn list-drifts
  "TxLog filtering processor: produce entries w/ issues in ALG/ATX.

  It checks the :ctrl header for issues and only passes borked entries. Returns
  lazy sequence of entries."
  {:added  "0.3.0"
   :static true}
  [entries]
  (when-let [curr (first entries)]
    (let [{:keys [at-ok? rows-ok?]} (:ctrl curr)]
      (if (or (not at-ok?) (not rows-ok?))
        (cons curr (lazy-seq (list-drifts (rest entries))))
        (recur (rest entries))))))

(defn correct-drifts
  "TxLog processor corrects the ALG to match ATX.

  Returns fixed :raw lines from ALG. It does not return ALG header lines!

  It attaches original entry as meta, so reporting can be done."
  {:added  "0.3.0"
   :static true}
  [entries]
  (letfn [(swap-at [line at]
            (re-replace line #"Row\s\[\s\d+\s\]" (format "Row [ %d ]" at)))
          (swap-rows [line rows]
            (re-replace line #"\[\s\d+\s\]\sRows" (format "[ %d ] Rows" rows)))]
    (when-let [entry (first entries)]
      (let [{:keys [at-ok? at-drift rows-ok? rows-drift]} (:ctrl entry)
            head-at       (-> entry :head :at)
            head-rows     (-> entry :head :rows)
            [line1 line2] (-> entry :head :raw)
            line2-at      (if-not at-ok?
                            (swap-at line2 (- head-at at-drift))
                            line2)
            line2-rows    (if-not rows-ok?
                            (swap-rows line2-at (- head-rows rows-drift))
                            line2-at)]
        (cons (with-meta [line1 line2-rows] entry)
              (lazy-seq (correct-drifts (rest entries))))))))

(defn correct-alg-drifts!
  "Write ALG copy with drifts fixed. Grabs file header and copies that as well.

  Make sure you don't point target to source.

  The interceptor will get fed result of correct-drifts with two ALG header
  lines cons'd. Should pass all inputs through, is for side effects, like
  logging, and defaults to identity.

  Please be aware that this only works when drifts are caused by ALG entries
  that are not reflecting count of ATX ones. If any entries in any file were
  deleted or swapped places -- that is if the order of entries or their
  matching was affected -- this 'fix' will not be what you want. You might
  get an OK looking file, but its audit value is limited. Consider yourself
  warned."
  {:added  "0.3.0"
   :static true}
  ([source-alg source-atx target-alg interceptor]
  (let [header ((io/lines {:processor (comp vec (partial take 2))}) source-alg)
        proc   (comp (io/sink (io/lines {:final-eol true}) target-alg)
                     flatten
                     interceptor
                     (partial cons header)
                     correct-drifts)]
    (when (= source-alg target-alg)
      (throw (ex-info "Cannot work in place! Source must differ from target!"
                      {:source source-alg
                       :target target-alg})))
    ((tx-log {:processor proc}) {:alg source-alg :atx source-atx})))
  ([source-alg source-atx target-alg]
   (correct-alg-drifts! source-alg source-atx target-alg identity)))

;; reporting on SSAudit -- filtering by criteria

(defn between?
  "Returns predicate that checks :head :timestamp against given interval.

  Format required is DateTimeFormatter/ISO_DATE_TIME - 2017-05-24T21:08:28.458Z"
  {:added  "0.1.0"
   :static true}
  [date-time-from date-time-to]
  (let [fmt      (.withLocale DateTimeFormatter/ISO_DATE_TIME Locale/US)
        from     (jt/local-date-time fmt date-time-from)
        to       (jt/local-date-time fmt date-time-to)]
    (fn between-checker [entry]
      (let [t (-> entry :head :timestamp)]
        (and (jt/after? t from) (jt/before? t to))))))

(defn user-matches?
  "Returns predicate that checks :head :user against given regexp."
  {:added  "0.1.0"
   :static true}
  [re-term]
  (fn user-match-checker [entry]
    (let [d (-> entry :head :user)]
      (try
        (re-find re-term d)
        (catch Exception ex (throw (ex-info "Oops in user-match-checker!"
                                            {:entry entry :re re-term}
                                            ex)))))))

(defn data-matches?
  "Returns predicate that checks :data :block with given regexp."
  {:added  "0.1.0"
   :static true}
  [re-term]
  (fn data-match-checker [entry]
    (let [d (-> entry :data :block)]
      (try
        (some (comp (partial re-find re-term) second) d)
        (catch Exception ex (throw (ex-info "Ops in data-match-checker!"
                                            {:entry entry :re re-term}
                                            ex)))))))

;; Writing out selected entries merged for presentation

(defn printable
  "Packs :head :raw and :data :block into ALG+ATX form.

  Entry header occupies first two rows and first column, data is shifted one
  column to the right. It can be cleanly dumped as CSV/TSV and imported
  into a spreadsheet.

  Called without arguments returns a transducer."
  {:added  "0.1.0"
   :static true}
  ([]
   (letfn [(header [entry]
             (->> entry :head :raw (mapv vector)))
           (shift [lines]
             (mapv (partial into [""]) lines))
           (body [entry]
             (let [lines (mapv second (-> entry :data :block))
                   lump  (string/join "\n" lines)
                   rows  (csv/parse-csv lump :delimiter \space :strict true)]
               (into (shift rows) [[""] [""]])))
           (pack-it [entry]
             (into (header entry) (body entry)))]
     (mapcat pack-it)))
  ([picks]
   (sequence (printable) picks)))
