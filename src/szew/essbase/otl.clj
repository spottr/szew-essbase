; Copyright (c) Sławek Gwizdowski
;
; Permission is hereby granted, free of charge, to any person obtaining
; a copy of this software and associated documentation files (the "Software"),
; to deal in the Software without restriction, including without limitation
; the rights to use, copy, modify, merge, publish, distribute, sublicense,
; and/or sell copies of the Software, and to permit persons to whom the
; Software is furnished to do so, subject to the following conditions:
;
; The above copyright notice and this permission notice shall be included
; in all copies or substantial portions of the Software.
;
; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
; OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
; FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
; THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
; LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
; FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
; IN THE SOFTWARE.
;
(ns ^{:author "Sławek Gwizdowski"
      :doc "Essbase XML Outline export.

Provides szew.io/XML processors for dimension extraction and convenience
functions consuming the outline export file directly.

Allows both sequencing and zipping over dimensions and members.

Just keep in mind that parsing big, deeply nested XMLs is a memory hog."}
 szew.essbase.otl
 (:require
   [clojure.zip :as zip]
   [szew.io :as io]
   [szew.io.util :refer [deep-sort]]
   [camel-snake-kebab.extras :refer [transform-keys]]
   [camel-snake-kebab.core :refer [->kebab-case-keyword]]
   [clojure.spec.alpha :as s]
   [szew.essbase.otl.member :as mbr]
   [szew.essbase.otl.dimension :as dim]))


;; Helpers

(defn make-good
  "Transform keys in map into kebab-case keywords."
  {:added  "0.1.0"
   :static true}
  [a-map]
  (transform-keys ->kebab-case-keyword a-map))

(defn list-content-tags
  "List all content tags visible from root of XML."
  {:added  "0.1.0"
   :static true}
  [xml-node]
  (mapv :tag (:content xml-node)))

;; Working with things that have :members, like parsed <Dimension> or <Member>

(s/def ::mbr/root (s/keys :req-un [::mbr/members]))

(defn member-zipper
  "Given processed dimension or member, return a zipper."
  {:added  "0.2.0"
   :static true}
  [root]
  (zip/zipper (comp seq :members)
              :members
              (fn [node children] (assoc node :members (vec children)))
              root))

(s/fdef
  member-zipper
  :args (s/cat :root ::mbr/root)
  :ret any?)

(defn member-walk
  "Given processed dimension visit and maybe modify members.

  Depth-First. Does visit root.

  That visitor callable should accept loc and return loc."
  {:added  "0.1.0"
   :static true}
  [visitor root]
  (loop [loc (member-zipper root)]
    (if (zip/end? loc)
      (zip/root loc)
      (recur (zip/next (visitor loc))))))

(s/fdef
  member-walk
  :args (s/cat :visitor any? :root ::mbr/root)
  :ret any?)

(defn member-seq
  "Your root into lazy-seq of members! Root first. Depth-First from there.

  Note: if you feed it a dimension -- you will get a dimension first.

  If you give it a husk of {:members seq-of-dimensions} you'll get that first."
  {:added  "0.2.0"
   :static true}
  [{:keys [members] :as root}]
  (when root
    (cons root (lazy-seq (mapcat member-seq members)))))

(s/fdef
  member-seq
  :args (s/cat :root ::mbr/root)
  :ret (s/* (s/alt :root ::mbr/root :member ::mbr/member)))

;; TODO: get meta from Element when it becomes available (alpha feature)
(defn seed-member
  "Given parsed XML <Member> node returns processed single member."
  {:added  "0.2.0"
   :static true}
  [node]
  (let [alias-pair (juxt (comp :table :attrs) (comp first :content))
        attr-pair  (juxt (comp :dimension :attrs) (comp :name :attrs))
        content    (group-by :tag (:content node))
        aliases    (into {} (map alias-pair (:Alias content)))
        udas       (set (mapcat :content (:UDA content)))
        attrs      (into {} (map attr-pair (:AttributeMember content)))
        children   (count (:Member content))
        node-attrs (make-good (:attrs node))]
    (mbr/->OutlineMember
     (str (first (keep aliases ["Default", "Long Names"])))
     aliases
     attrs
     (get node-attrs :consolidation "+")
     (get node-attrs :data-storage "StoreData")
     (= 0 children)
     nil
     (get node-attrs :name)
     (= "Y" (get node-attrs :shared-member))
     :Member
     (list-content-tags node)
     (= "Y" (get node-attrs :two-pass-calc))
     udas
     "?"
     nil
     (get node-attrs :member-formula
          (-> content :MemberFormula first :content first)) ;; 11.2 change!
     nil
     nil
     (get node-attrs :time-balance)
     (get node-attrs :variance-reporting)
     (get node-attrs :storage-category)
     (get node-attrs :hierarchy-type)
     (into (dissoc content :Alias :Member :MemberFormula :AttributeMember)
           (reduce dissoc node-attrs mbr/member-keys)))))

(s/fdef
  seed-member
  :ret ::mbr/member)

(defn expand-member
  "Given parsed XML <Member> root returns processed member with descendants.

  Expecting at to be path above root, defaults to [\"?\"].

  Adds :generation, :dimension and :parent to each member."
  {:added  "0.2.0"
   :static true}
  ([at root]
   (let [pq       (clojure.lang.PersistentQueue/EMPTY)
         children (comp (partial into pq
                                 (filter (comp (partial = :Member) :tag)))
                        :content)
         start    (assoc (seed-member root)
                         :path       at
                         :generation (count at)
                         :dimension  (first at)
                         :parent     (last at))
         push     (fn push [path child]
                    (let [node  (seed-member child)
                          up    (peek path)
                          node+ (assoc node
                                       :path       (conj (:path up) (:name up))
                                       :generation (inc (:generation up))
                                       :dimension  (first (:path up))
                                       :parent     (:name up))]
                      (conj path node+)))]
     (loop [path  [start]
            stack [(children root)]]
       (let [node (peek path)       ;; current member
             todo (peek stack)]     ;; current children to visit
         (if (seq todo) ;; work on next child
           (recur (push path (peek todo))
                  (conj (pop stack)      ;; todo off the stack
                        (pop todo)  ;; append modified todo
                        (children (peek todo)))) ;; link more children!
           (if (seq (pop path)) ;; we're not done yet
             (let [up        (peek (pop path))
                   magnitude (pop (pop path))]
               (recur (conj magnitude (update up :members (fnil conj []) node))
                      (pop stack)))
             node))))))
  ([root]
   (expand-member ["?"] root)))

(s/fdef
  expand-member
  :ret ::mbr/member)

;; TODO: get meta from Element when it becomes availlable (alpha feature)
(defn seed-dimension
  "Given parsed XML <Dimension> node returns processed dimension."
  {:added  "0.2.0"
   :static true}
  [node]
  (let [gen-name   (juxt #(Integer/parseInt (:number %)) :name)
        content    (group-by :tag (:content node))
        gen-level  (comp (partial into (sorted-map))
                         (partial map (comp gen-name :attrs))
                         (partial filter (comp (partial = :GenLevel) :tag))
                         (partial mapcat :content))
        attributes (->> (:AttributeDimension content)
                        (map (comp :name :attrs))
                        set)
        attrs      (make-good (:attrs node))]
    (dim/->OutlineDimension
     attributes
     (get attrs :csversion "?")
     (get attrs :data-storage "StoreData")
     (get attrs :density "Sparse")
     (get attrs :dimension-type "Standard")
     (gen-level (:Generations content))
     (get attrs :hierarchy-type)
     (= "Y" (get attrs :is-compression-dimension))
     (gen-level (:Levels content))
     nil
     (get attrs :name)
     :Dimension
     (list-content-tags node)
     (get attrs :attribute-type)
     (get attrs :storage-category)
     (into (dissoc content :AttributeDimension :Generations :Levels :Member)
           (reduce dissoc attrs dim/dimension-keys)))))

(s/fdef
  seed-dimension
  :ret ::dim/dimension)

(defn expand-dimension
  "Given parsed XML <Dimension> returns processed dimension with descendants."
  {:added  "0.2.0"
   :static true}
  [node]
  (let [root         (seed-dimension node)
        members      (->> (:content node)
                          (filter (comp (partial = :Member) :tag))
                          (mapv (partial expand-member [(:name root)])))
        expanded     (assoc root :members members)
        member-count (dec (count (member-seq expanded)))]
    (assoc expanded :member-count member-count)))

(s/fdef
  expand-dimension
  :ret ::dim/dimension)

(defn list-dimensions
  "Given parsed XML returns dimensions without members. Lazy!"
  {:added  "0.1.0"
   :static true}
  [xml-root]
  (->> (:content xml-root)
       (filter (comp (partial = :Dimension) :tag))
       (map seed-dimension)))

(defn list-dimensions!
  "Given a XML file returns dimensions wihtout members. Lazy!

  Callable mf is composed with list-dimensions, vec by default."
  {:added  "0.2.0"
   :static true}
  ([mf xml-file]
   (io/in! (io/xml {:processor (comp mf list-dimensions)}) xml-file))
  ([xml-file]
   (list-dimensions! vec xml-file)))

(defn extract-dimensions
  "Given parsed XML extracts dimensions with `expand-dimension`. Lazy!

  Predicate `wanted?` will be fed output of `seed-dimension`, no `:members`!"
  {:added  "0.1.0"
   :static true}
  ([xml-root]
   (->> (:content xml-root)
        (filter (comp (partial = :Dimension) :tag))
        (map expand-dimension)))
  ([wanted? xml-root]
   (->> (:content xml-root)
        (filter (comp (partial = :Dimension) :tag))
        (filter (comp wanted? seed-dimension))
        (map expand-dimension))))

(defn extract-dimensions!
  "Returns expanded dimensions. Allows predicate on unexpanded dimension.

  The `wanted?` predicate is true by default, mf callable is vec by default."
  {:added  "0.2.0"
   :static true}
  ([wanted? mf xml-file]
   (let [proc (comp mf (partial extract-dimensions wanted?))]
     (io/in! (io/xml {:processor proc}) xml-file)))
  ([mf xml-file]
   (extract-dimensions! (constantly true) mf xml-file))
  ([xml-file]
   (extract-dimensions! (constantly true) vec xml-file)))

(defn member-name-set
  "Given root with :members returns set of member names, including root."
  {:added  "0.2.0"
   :static true}
  [root]
  (set (map :name (member-seq root))))

(defn prepare-lut
  "Given list of <Dimension>s prepares a LUT hash map of `{member dimension}`.

  Attached `meta` contains:
  * :dimensions - parsed dimensions (with emtpy :members),
  * :rlut - {dimension #{members}} structure for top-down lookup,
  * :total-dims - total number of dimensions given,
  * :attr-dims - number of attribute dimensions given,
  * :data-dims - number of non-attribute dimensions given (used in data files)."
  {:added  "0.2.0"
   :static true}
  [dimensions]
  (let [rlut  (into {} (map (juxt :name member-name-set) dimensions))
        lutfn (fn [[k vs]] (map (juxt identity (constantly k)) vs))
        lut   (into {} (mapcat lutfn rlut))
        total (count dimensions)
        attrs (->> dimensions
                   (filter (comp (partial = "Attribute") :dimension-type))
                   (count))
        data  (- total attrs)]
    (vary-meta lut
               assoc
               :rlut       rlut
               :dimensions (mapv #(assoc % :members []) dimensions)
               :total-dims total
               :attr-dims  attrs
               :data-dims  data)))

(s/def
  ::lut
  (s/or :simple (s/map-of string? string?)
        :tricky (s/map-of string? any?)))

(s/fdef
  prepare-lut
  :args (s/cat :dimensions (s/coll-of ::dim/dimension))
  :ret ::lut)

(defn member-lut
  "Given parsed XML returns function of member name to dimension name.

  Generated look-up-table can be found in function's meta."
  {:added  "0.1.0"
   :static true}
  ([xml-root]
   (prepare-lut (extract-dimensions xml-root)))
  ([wanted? xml-root]
   (prepare-lut (extract-dimensions wanted? xml-root))))

(s/fdef
  member-lut
  :args (s/alt :1-arg  (s/cat :node any?)
               :2-args (s/cat :pred (s/alt :fn fn? :ifn ifn?) ;; s/fspec?
                              :node any?))
  :ret  ::lut)

(defn member-lut!
  "Given outline export returns function of member name to dimension name.

  See doc of `prepare-lut` for details."
  {:added  "0.2.0"
   :static true}
  ([xml-file]
   (extract-dimensions! prepare-lut xml-file))
  ([wanted? xml-file]
   (extract-dimensions! (comp prepare-lut (partial filter wanted?)) xml-file)))

(s/fdef
  member-lut!
  :args (s/alt :1-arg  (s/cat :path string?)
               :2-args (s/cat :pred (s/alt :fn fn? :ifn ifn?) ;; s/fspec?
                              :path string?))
  :ret  ::lut)

(defn non-attr-dim
  "Predicate over a Dimension: negation of storage-category = Attribute."
  {:added  "0.2.0"
   :static true}
  [dimension]
  (not= "Attribute" (:storage-category dimension)))

(s/fdef
  non-attr-dim
  :args (s/cat :dimension ::dim/dimension)
  :ret  boolean?)

(defn data-member-lut!
  "Skip Attribute dimension when preparing the lut."
  {:added  "0.2.0"
   :static true}
  ([xml-file]
   (member-lut! non-attr-dim xml-file)))

(s/def ::m->d ::lut)

(s/def ::dimensions (s/coll-of string? :distinct true :into []))

(s/def ::dim-count (s/and int? pos?))

(s/def
  ::lut-package
  (s/keys :req-un [::m->d ::dimensions ::dim-count]))

(defn member-lut-package!
  "Prepares a hash-map with :m->d and :dim-count based on given file.

  Also includes dimension names in :dimensions."
  {:added  "0.2.0"
   :static true}
  ([xml-file]
   (let [lut (member-lut! xml-file)]
     {:m->d       lut
      :dimensions (mapv :name (:dimensions (meta lut)))
      :dim-count  (:data-dims (meta lut))}))
  ([wanted? xml-file]
   (let [lut (member-lut! wanted? xml-file)]
     {:m->d       lut
      :dimensions (mapv :name (:dimensions (meta lut)))
      :dim-count  (:data-dims (meta lut))})))

(s/fdef
  member-lut-package
  :args (s/alt :1-arg  (s/cat :path string?)
               :2-args (s/cat :pred (s/alt :fn fn? :ifn ifn?) ;; s/fspec?
                              :path string?))
  :ret  ::lut-package)

;; Working with <cube>

(defn read-cube
  "Given parsed XML <cube> node returns processed map."
  {:added  "0.2.0"
   :static true}
  [node]
  (let [content  (group-by :tag (:content node))
        app-conf (-> content :applicationSettings first :attrs)
        db-conf  (-> content :databaseSettings first :content first :attrs)
        alias-ts (set (map (comp :name :attrs)
                           (filter (comp (partial = :Table) :tag)
                                   (:content (first (:aliasTables content))))))
        def-at   (->> (:aliasTables content)
                      first
                      :content
                      (filter (comp (partial = :DefaultTable) :tag))
                      first
                      :attrs
                      :name)
        dtss     (->> (:dynamicTimeSeriesSettings content)
                      first
                      :content
                      (map (juxt :tag :attrs))
                      (into {}))
        attrs    (->> (:attributeSettings content)
                      first
                      :content
                      (map (juxt :tag (comp first :content)))
                      (into {}))
        acs      (->> (:attributeCalculationSettings content)
                      first
                      :content
                      (map (juxt :tag (comp make-good :attrs)))
                      (into {}))]
    (-> (:attrs node)
        (make-good)
        (assoc :tag :cube)
        (assoc :contained (list-content-tags node))
        (assoc :application-settings (make-good app-conf))
        (assoc :database-settings (make-good db-conf))
        (assoc :alias-tables alias-ts)
        (assoc :default-alias-table def-at)
        (assoc :dynamic-time-series-settings dtss)
        (assoc :attribute-settings (make-good attrs))
        (assoc :attribute-calculation-settings (make-good acs))
        (deep-sort))))

(defn cube
  "Given parsed XML root node finds first <cube> and returns a map."
  {:added  "0.2.0"
   :static true}
  [xml-root]
  (->> (:content xml-root)
       (filter (comp (partial = :cube) :tag))
       (first)
       (read-cube)))

(defn cube!
  "Given XML outline export file returns parsed <cube> as a map."
  {:added  "0.2.0"
   :static true}
  [xml-file]
  (io/in! (io/xml {:processor cube}) xml-file))

;; Working with <smartLists>

;; TODO: Implement?
(defn smart-lists
  "Not Implemented! Given parsed XML returns <smartLists> processed."
  {:added  "0.1.0"
   :static true}
  [xml-root]
  :not-implemented)

;; Shims for 0.2.0

(def ^{:doc        "Compatibility shim for member-zipper."
       :added      "0.1.0"
       :deprecated "0.2.0"}
  zipper member-zipper)

(def ^{:doc        "Compatibility shim for seed-member."
       :added      "0.1.0"
       :deprecated "0.2.0"}
  member seed-member)

(def ^{:doc        "Compatibility shim for expand-member."
       :added      "0.1.0"
       :deprecated "0.2.0"}
  expanded-member expand-member)

(def ^{:doc        "Compatibility shim for seed-dimension."
       :added      "0.1.0"
       :deprecated "0.2.0"}
  dimension seed-dimension)

(def ^{:doc        "Compatibility shim for expand-dimension."
       :added      "0.1.0"
       :deprecated "0.2.0"}
  expanded-dimension expand-dimension)

(def ^{:doc        "Compatibility shim for member-name-set."
       :added      "0.1.0"
       :deprecated "0.2.0"}
  member-set member-name-set)

(def ^{:doc        "Compatibility shim for prepare-lut."
       :added      "0.1.0"
       :deprecated "0.2.0"}
  lut-fn prepare-lut)

(def ^{:doc        "Compatibility shim for member-lut-package!"
       :added      "0.1.0"
       :deprecated "0.2.0"}
  lut-package member-lut-package!)
