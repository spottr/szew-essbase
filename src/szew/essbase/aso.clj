; Copyright (c) Sławek Gwizdowski
;
; Permission is hereby granted, free of charge, to any person obtaining
; a copy of this software and associated documentation files (the "Software"),
; to deal in the Software without restriction, including without limitation
; the rights to use, copy, modify, merge, publish, distribute, sublicense,
; and/or sell copies of the Software, and to permit persons to whom the
; Software is furnished to do so, subject to the following conditions:
;
; The above copyright notice and this permission notice shall be included
; in all copies or substantial portions of the Software.
;
; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
; OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
; FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
; THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
; LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
; FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
; IN THE SOFTWARE.
;
(ns ^{:author "Sławek Gwizdowski"
      :doc "Essbase ASO export.

What to expect from input:

* Space separated.
* Quoted member names, non-quoted values.
* Variable column count, last column is always a value, every line is a cell.
* First line is a complete POV, other lines do minimal POV update only.
* So this whole file must be parsed in order...
* ... and all members must be mapped to dimensions properly.

To parse the export file you need to know one thing: the complete member
to dimension mapping of storage dimensions.

It's easy to get member names from a data file: just drop the last column
in every row."}
 szew.essbase.aso
 (:require
   [szew.io :as io]
   [clojure.java.io :as clj.io :refer [reader]]
   [clojure-csv.core :as csv]
   [clojure.spec.alpha :as s])
 (:import
   [java.io BufferedReader]
   [clojure.lang IFn]))

(s/def ::dim-count (s/and int? pos?))

(s/def
  ::members-args
  (s/or :none (s/nilable (s/and map? empty?))
        :some (s/keys :opt-un [:io/processor :io/encoding])))

(s/def
  ::members
  (s/keys :req-un [:io/processor :io/encoding]))

(defrecord Members [processor encoding]
  io/Input
  (io/in! [spec source]
    (when-not (s/valid? ::members spec)
      (throw
        (ex-info "Members spec validation failed!"
                 {:explanation (s/explain-data ::members spec)
                  :source      source})))
    (io! "Reading files here!"
         (with-open [^BufferedReader r (reader source :encoding encoding)]
           (processor (mapcat butlast (csv/parse-csv r :delimiter \space))))))
  IFn
  (applyTo [spec args]
    (when-not (= (count args) 1)
      (throw (ex-info "Wrong number of arguments! Expected 1."
                      {:spec spec
                       :args args})))
    (io/in! spec (first args)))
  (invoke [spec source]
    (io/in! spec source)))

(defn members
  "Processor gets member name per every occurrence in file.

  Default processor creates a hash-set of member names."
  {:added  "0.1.0"
   :static true}
  ([]
   (Members. set "UTF-8"))
  ([spec]
   (into (members) spec))
  ([spec source]
   (io/in! (members spec) source)))

(s/fdef
  members
  :args (s/alt :0-args (s/cat)
               :1-arg  (s/cat :spec ::members-args)
               :2-args (s/cat :spec ::members-args :source any?))
  :ret  (s/or :members (s/and (partial instance? Members) ::members)
              :other   any?))

(s/def
  ::cells-args
  (s/or :none (s/nilable (s/and map? empty?))
        :some (s/keys :opt-un [:io/processor :otl/m->d :io/encoding])))

(s/def
  ::cells
  (s/keys :req-un [:io/processor :otl/m->d :io/encoding]))

(defrecord Cells [processor m->d encoding]
  io/Input
  (io/in! [spec source]
    (when-not (s/valid? ::cells spec)
      (throw
        (ex-info "Cells spec validation failed!"
                 {:explanation (s/explain-data ::cells spec)
                  :source      source})))
    (letfn [(row->cell [idx row]
              (with-meta [(->> (butlast row)
                               (mapv (juxt m->d identity))
                               (into (hash-map)))
                          (last row)]
                {:line idx :source source :row row}))
            (ok? [cell]
              (if (contains? (first cell) nil)
                (throw (ex-info "Unknown dimension!"
                                {:cell cell
                                 :line (:line (meta cell))}))
                cell))
            (reductioner [prev curr]
              (with-meta [(merge (first prev) (first curr))
                          (last curr)]
                         (meta curr)))]
      (io! "Reading files here!"
           (with-open [^BufferedReader r (reader source :encoding encoding)]
             (->> (csv/parse-csv r :delimiter \space)
                  (map-indexed row->cell)
                  (map ok?)
                  (reductions reductioner)
                  (processor))))))
  IFn
  (applyTo [spec args]
    (when-not (= (count args) 1)
      (throw (ex-info "Wrong number of arguments! Expected 1."
                      {:spec spec
                       :args args})))
    (io/in! spec (first args)))
  (invoke [spec source]
    (io/in! spec source)))

(defn cells
  "Processor will get data point per each cell in file.

  Requires complete member to dimension mapping in m->d.

  Processor gets seq of [{dimension member} value].

  Default processor will return a vector of cells.

  Errors out with ex-info if any member is mapped to nil."
  {:added  "0.1.0"
   :static true}
  ([]
   (Cells. vec {} "UTF-8"))
  ([spec]
   (into (cells) spec))
  ([spec source]
   (io/in! (cells spec) source)))

(s/fdef
  cells
  :args (s/alt :0-args (s/cat)
               :1-arg  (s/cat :spec ::cells-args)
               :2-args (s/cat :spec ::cells-args :source any?))
  :ret  (s/or :cells (s/and (partial instance? Cells)
                            (s/keys :req-un [:io/processor :io/encoding]))
              :other any?))

;; Helper functions -- Members

(defn sniff-unknown
  "Creates a processor for Members that will return a seq of distinct missing
  members."
  {:added  "0.1.0"
   :static true}
  [m->d]
  (fn distinct-unknown [member]
    (distinct (filterv (comp nil? m->d) member))))

(defn sniff-dimensions
  "A processor for Cells that will return dimensions of first cell."
  {:added  "0.1.0"
   :static true}
  [cells]
  (set (keys (ffirst cells))))

;; Convert dumps into single value column file

(defn dump->tsv
  "Given dump files consolidates them into single, row-expanded TSV."
  {:added  "0.1.0"
   :static true}
  ([m->d order out-path in-path & in-paths]
   (let [->row (fn [cell] (conj (mapv (first cell) order) (last cell)))
         sink  (io/sink (io/tsv) out-path)
         suck  (cells {:processor (comp sink (partial map ->row))
                       :m->d m->d})]
     (io/in! suck in-path)
     (when (seq in-paths)
       (let [sink+ (io/sink (io/tsv {:append true}) out-path)
             suck+ (cells {:processor (comp sink+ (partial map ->row))
                           :m->d m->d})]
         (doseq [in-path in-paths]
           (io/in! suck+ in-path)))))))
