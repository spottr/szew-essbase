; Copyright (c) Sławek Gwizdowski
;
; Permission is hereby granted, free of charge, to any person obtaining
; a copy of this software and associated documentation files (the "Software"),
; to deal in the Software without restriction, including without limitation
; the rights to use, copy, modify, merge, publish, distribute, sublicense,
; and/or sell copies of the Software, and to permit persons to whom the
; Software is furnished to do so, subject to the following conditions:
;
; The above copyright notice and this permission notice shall be included
; in all copies or substantial portions of the Software.
;
; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
; OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
; FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
; THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
; LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
; FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
; IN THE SOFTWARE.
;
(ns ^{:author "Sławek Gwizdowski"
      :doc "Essbase application log and MaxL spool.

# Application logs

Log timestamp line looks like:

  [Tue Nov 06 08:50:26 2001]Local/Sample///Info(1013214)

So it's [timestamp]Local/application/database/issuer/type(code) more or less.

And then data follows, that looks like this:

  Clear Active on User [admin] Instance [1];

So this one contains user info, but it could be Command [.+]
or Database [.+] etc.

TODO: break entries into fields, not only headers?

Fields you'll get using AppLog:

* full timestamp, decoded from date
* date: yyyy-mm-dd decoded from timestamp
* application: String
* database: String
* user: String
* level: Info | Warning | Error | ???
* code: int
* raw: String, full payload of the entry (head + message)

Additional tables of use: Code categories [1].

# MaxL shell spool

And then there's MaxL allowing you to see all the useful system properties,
and even run MDX queries and get results back, almost like in a real database,
but only if you remember to set the column_width just right, or it will
truncate those space padded, fixed width table outputs.

FIXME: can headers in MaxL output be multiline?

I just set it to 256 and that's the default value here. YMMV.

That MaxLSpool will help you extract those, remove the padding and pack columns
into hash maps. Will also keep MaxL output preceding and following tabular
output.

Some 'special' values are resolvable via maxl-constants map. It's WIP with
little P.

[1] https://docs.oracle.com/cd/E12825_01/epm.111/esb_dbag/dlogs.htm"}
 szew.essbase.logs
 (:require
  [java-time :as jt]
  [szew.io :as io]
  [szew.io.util :refer [fixed-width-split vecs->maps]]
  [camel-snake-kebab.core :as c-s-k]
  [clojure.java.io :as clj.io :refer [reader]]
  [clojure.spec.alpha :as s])
 (:import
  [java.util Locale]
  [java.time.format DateTimeFormatter]
  [java.io BufferedReader]
  [clojure.lang IFn]))

(defrecord AppLog [processor timestamp locale]
  io/Input
  (in! [_ source]
    (letfn [(->entry [raw-entry]
              (let [ts-in  (DateTimeFormatter/ofPattern timestamp locale)
                    ts-out (.withLocale DateTimeFormatter/ISO_DATE_TIME locale)
                    head-re #"(?xi)^\[([^\]]+)\]  # timestamp
                                    ([^/]*)/      # source?
                                    ([^/]*)/      # app
                                    ([^/]*)/      # db
                                    ([^/]*)/      # user
                                    (?:([^/]*)/|) # unk
                                    ([^/]*)       # level
                                    \((\d+)\)$    # code"
                    head-fi (->> raw-entry
                                 first
                                 second
                                 (re-find head-re)
                                 rest
                                 (mapv str))
                    entry (-> [:mark :source :app :db :user :unk :level :code]
                              (zipmap head-fi)
                              (update :code #(Integer/parseInt %))
                              (assoc :raw (mapv second raw-entry)))
                    d-t (jt/local-date-time ts-in (:mark entry))
                    t-s (jt/format ts-out d-t)]
                (with-meta (assoc entry :datetime d-t :timestamp t-s)
                  {:head-line (-> raw-entry first first)
                   :raw-entry raw-entry
                   :source source})))
            (bracket? [^String line]
              (.startsWith line "["))
            (entries [a-line-seq]
              (->> a-line-seq
                   (map-indexed vector)
                   (filter (comp (partial not= "") second))
                   (partition-by (comp bracket? second))
                   (partition 2 2 nil)
                   (map (comp (partial apply into)
                              (juxt (comp vec first) (comp vec second))))
                   (map ->entry)))]
      (io! "Reading file!"
        (with-open [^BufferedReader r (reader source :encoding "UTF-8")]
          (processor (entries (line-seq r)))))))
  IFn
  (applyTo [spec args]
    (when-not (= (count args) 1)
      (throw (ex-info "Wrong number of arguments! Expected 1."
                      {:spec spec
                       :args args})))
    (io/in! spec (first args)))
  (invoke [spec target]
    (io/in! spec target)))

(defn app-log
  "Just zip entry headers with their payload, then feed to the processor."
  {:added  "0.1.0"
   :static true}
  ([]
   (AppLog. vec "EEE MMM dd HH:mm:ss yyyy" Locale/US))
  ([spec]
   (into (app-log) spec))
  ([spec source]
   (io/in! (app-log spec) source)))

;; MaxL

(defn maxl-prompt?
  "Tries to find MaxL shell prompt in the beginning of given line."
  {:added  "0.1.0"
   :deprecatd "0.3.3"
   :static true}
  [^String line]
  (re-matches #"(?xi)^(MAXL>\s+).*" line))

(s/def ::column-width (s/and int? pos?))

(s/def
  ::maxl-spool-args
  (s/or :none (s/nilable (s/and map? empty?))
        :some (s/keys :opt-un [:io/processor ::column-width :io/encoding])))

(s/def
  ::maxl-spool
  (s/keys :req-un [:io/processor ::column-width :io/encoding]))

(defrecord MaxLSpool [processor column-width encoding prompt-re]
  io/Input
  (in! [spec source]
    (letfn [(table? [^String row] (zero? (rem (count row) column-width)))
            (trim [^String slice] (.trim slice))
            (trimmer [row] (mapv trim row))
            (splitter [data]
              ;; Check how many columns there are: (len / width),
              ;; then repeat this width as many times as required.
              ;; Finally create a trimmer based on fixed-width-splits.
              (let [columns (/ (count (first data)) column-width)
                    widths  (vec (repeat columns column-width))]
                (comp trimmer (fixed-width-split widths))))
            (rip [block]
              (if-not (first (filter (comp table? second) block))
                (with-meta {:head (mapv second block), :body [], :tail []}
                           {:head-line (-> block first first),
                            :source source,
                            :block block})
                (let [[head data tail] (partition-by (comp not table? second)
                                                     block)
                      slicer (splitter (map second data))
                      fields (->> data
                                  first
                                  second
                                  slicer
                                  (mapv c-s-k/->kebab-case-keyword))
                      entries (->> data
                                   (map (comp slicer second))
                                   (drop 2) ;; header and -+- row
                                   (vecs->maps fields))]
                  (with-meta {:head (trimmer (mapv second head)),
                              :data (vec entries),
                              :tail (trimmer (mapv second tail))}
                             {:head-line (ffirst head),
                              :data-line (ffirst data),
                              :tail-line (ffirst tail),
                              :source    source,
                              :block     block}))))
            (splice [lines]
              (let [prompt? (comp (partial re-matches prompt-re) second)
                    blocks  (->> lines
                                 (map vector (range))
                                 (filter (comp not empty? second))
                                 (drop-while (comp not prompt?))
                                 (partition-by prompt?)
                                 (partition 2 2 nil)
                                 (map (comp (partial apply into)
                                            (juxt (comp vec first)
                                                  (comp vec second)))))]
                (map rip blocks)))]
      (when-not (s/valid? ::maxl-spool spec)
        (throw
          (ex-info "MaxLSpool spec validation failed!"
                   {:explanation (s/explain-data ::maxl-spool spec)
                    :source      source})))
      (io! "Reading files here!"
        (with-open [^BufferedReader r (reader source :encoding encoding)]
          (processor (splice (line-seq r)))))))
  IFn
  (applyTo [spec args]
    (when-not (= (count args) 1)
      (throw (ex-info "Wrong number of arguments! Expected 1."
                      {:spec spec
                       :args args})))
    (io/in! spec (first args)))
  (invoke [spec target]
    (io/in! spec target)))

(defn maxl-spool
  "Process MaxL spool files, packs are data from prompt to prompt.

  Returns:

  {:head [String], :data [{Keyword String}], :tail [String]}
  ;; or
  {:head [String], :data [], :tail []}

  MaxL spool setup:

  MAXL> set column_width 256;

  MAXL> set timestamp on;

  MAXL> spool on to '/your/file/location.log';

  And then process output:

  * Split by maxl-prompt? into blocks.
  * If no lines in block are multiple of column-width:
    - Head: just trimmed lines from the block
    - Data: empty vector
    - Tail: empty vector
  * If any lines in block are multiples of column-width:
    - Head: prompt (lines before wide lines)
    - Data: data (fixed-width tabular data, split, trimmed, recordified)
    - Tail: additional output at the end: INFO, WARNING, timestamp etc.

  Each returned map is having some meta attached, might want to take a peek."
  {:added  "0.1.0"
   :static true}
  ([]
   (MaxLSpool. vec 256 "UTF-8" #"(?xi)^(MAXL>\s+).*"))
  ([spec]
   (into (maxl-spool) spec))
  ([spec source]
   (io/in! (maxl-spool spec) source)))

(s/fdef
  maxl-spool
  :args (s/alt :0-args (s/cat)
               :1-arg  (s/cat :spec ::maxl-spool-args)
               :2-args (s/cat :spec ::maxl-spool-args :source any?))
  :ret  (s/or :max-spool (s/and (partial instance? MaxLSpool)
                                (s/keys :req-un [:io/processor :io/encoding]))
              :other     any?))

(def ^{:doc   "Some predefined values from Oracle docs."
       :added "0.1.0"
       :deprecated "0.3.3"}
 maxl-constants
  {:display-application
   {:application-type {"0" (str "Unspecified encoding type. "
                                "The application was created using a "
                                "pre-Release 7.0 version of Essbase.")
                       "1" "This value is not in use."
                       "2" "Non-Unicode-mode application"
                       "3" "Unicode-mode application"}
    :application-status {"0" "Not Loaded"
                         "1" "Loading"
                         "2" "Loaded"
                         "3" "Unloading"}
    :storage-type {"0" "Default data storage"
                   "1" "Multidimensional data storage"
                   "2" "DB2 relational data storage"}}
   :display-database
   {:db-type {"0" "Normal",
              "1" "Currency"}
    :currency-conversion {"1" "division",
                          "2" "multiplication"}
    :compression {"1" "RLE",
                  "2" "Bitmap",
                  "3" "ZLIB"}
    :pending-io-access-mode {"0" "Invalid-Error",
                             "1" "Buffered",
                             "2" "Direct"}
    :db-status {"0" "Not-Loaded",
                "1" "Loading",
                "2" "Loaded",
                "3" "Unloading"}
    :data-status {"0" "No Data",
                  "1" "Data Loaded Without Calculation",
                  "2" "Data is Calculated"}
    :request-type {"0" "Data Load",
                   "1" "Calculation",
                   "2" "Outline Update"} ;; otherwise "Unknown"
    :display-user
    {:application-access-type {"0" "No access"
                               "1" "Hyperion Essbase access"
                               "2" "Hyperion Planning access"
                               "3" "Essbase and Planning access"}
     :type {"0" "User is set up using native Essbase security."
            "1" (str "User is externally authenticated using custom "
                     "Essbase libraries.")
            "3" "User is externally authenticated using Shared Services."}}
    ;; TO BE CONTINUED, maybe.
}})
