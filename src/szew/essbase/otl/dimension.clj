; Copyright (c) Sławek Gwizdowski
;
; Permission is hereby granted, free of charge, to any person obtaining
; a copy of this software and associated documentation files (the "Software"),
; to deal in the Software without restriction, including without limitation
; the rights to use, copy, modify, merge, publish, distribute, sublicense,
; and/or sell copies of the Software, and to permit persons to whom the
; Software is furnished to do so, subject to the following conditions:
;
; The above copyright notice and this permission notice shall be included
; in all copies or substantial portions of the Software.
;
; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
; OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
; FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
; THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
; LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
; FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
; IN THE SOFTWARE.
;
(ns ^{:author "Sławek Gwizdowski"
      :doc "Dimension specs for otl."}
  szew.essbase.otl.dimension
  (:require [clojure.spec.alpha :as s]
            [szew.essbase.otl.member :as mbr]))

(s/def ::attribute-nodes (s/and set? (partial every? string?)))

(s/def ::attribute-type
  (s/nilable (s/or :known   #{"Boolean" "Date" "Numeric" "Text"}
                   :future? string?)))

(s/def ::csversion string?)

(s/def ::data-storage (s/or :known   #{"DynamicCalc" "DynamicCalcAndStore"
                                       "NeverShare" "LabelOnly" "StoreData"
                                       "SharedMember"}
                            :future? string?))

(s/def ::density (s/or :known   #{"Dense" "Sparse"}
                       :future? string?))

(s/def ::dimension-type (s/or :known    #{"Accounts" "Attribute" "Country"
                                          "Standard" "Time"}
                              :future? string?))

(s/def ::generations (s/map-of (s/and int? pos?) string?))

(s/def ::hierarchy-type (s/or :known   #{"Stored" "Dynamic" "Enabled"}
                              :future? string?))

(s/def ::is-compression-dimension boolean?)

(s/def ::levels (s/map-of (s/and int? (complement neg?)) string?))

(s/def ::storage-category
  (s/nilable (s/or :known   #{"Accounts" "Attribute" "Time"}
                   :future? string?)))

(s/def ::name (s/and string? (complement empty?)))

(s/def ::tag #{:Dimension})

(s/def ::dimension
  (s/keys :req-un [::attribute-nodes
                   ::csversion
                   ::data-storage
                   ::density
                   ::dimension-type
                   ::generations
                   ::hierarchy-type
                   ::is-compression-dimension
                   ::levels
                   ::mbr/members
                   ::name
                   ::tag
                   ::mbr/contained]
          :opt-un [::attribute-type
                   ::storage-category
                   ::mbr/misc]))

(defrecord OutlineDimension [attribute-nodes
                             csversion
                             data-storage
                             density
                             dimension-type
                             generations
                             hierarchy-type
                             is-compression-dimension
                             levels
                             members
                             name
                             tag
                             contained
                             attribute-type
                             storage-category
                             misc])

(def dimension-keys (keys (map->OutlineDimension nil)))
