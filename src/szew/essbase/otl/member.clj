; Copyright (c) Sławek Gwizdowski
;
; Permission is hereby granted, free of charge, to any person obtaining
; a copy of this software and associated documentation files (the "Software"),
; to deal in the Software without restriction, including without limitation
; the rights to use, copy, modify, merge, publish, distribute, sublicense,
; and/or sell copies of the Software, and to permit persons to whom the
; Software is furnished to do so, subject to the following conditions:
;
; The above copyright notice and this permission notice shall be included
; in all copies or substantial portions of the Software.
;
; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
; OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
; FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
; THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
; LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
; FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
; IN THE SOFTWARE.
;
(ns ^{:author "Sławek Gwizdowski"
      :doc "Member specs for otl."}
  szew.essbase.otl.member
  (:require [clojure.spec.alpha :as s]))

(s/def ::alias string?)

(s/def ::aliases (s/map-of string? string?))

(s/def ::attributes (s/map-of string? string?))

(s/def ::consolidation (s/or :known   #{"~" "+" "-" "*" "/" "^"}
                             :future? string?))

(s/def ::data-storage (s/or :known   #{"DynamicCalc" "DynamicCalcAndStore"
                                       "NeverShare" "LabelOnly" "StoreData"
                                       "ShareData"}
                            :future? string?))

(s/def ::dimension (s/and string? (complement empty?)))

(s/def ::generation (s/nilable (s/and int? pos?)))

(s/def ::level0? boolean?)

(s/def ::member-formula (s/nilable string?))

(s/def ::members (s/* ::member))

(s/def ::name (s/and string? (complement empty?)))

(s/def ::parent (s/nilable (s/and string? (complement empty?))))

(s/def ::path (s/* ::name))

(s/def ::shared-member boolean?)

(s/def ::tag #{:Member})

(s/def ::contained (s/coll-of keyword? :into [] :kind vec))

(s/def ::storage-category (s/nilable (s/or :known   #{"Attribute"}
                                           :future? string?)))

(s/def ::time-balance (s/nilable (s/or :known   #{"First" "Last" "Average"}
                                       :future? string?)))

(s/def ::two-pass-calc boolean?)

(s/def ::uda (s/and set? (partial every? string?)))

(s/def ::variance-reporting (s/nilable (s/or :known   #{"Expense" "Income"}
                                             :future? string?)))

(s/def ::root (s/keys :req-un [::members]))

(s/def ::misc any?)

(s/def ::member
  (s/keys :req-un [::alias
                   ::aliases
                   ::attributes
                   ::consolidation
                   ::data-storage
                   ::level0?
                   ::members
                   ::name
                   ::shared-member
                   ::tag
                   ::contained
                   ::two-pass-calc
                   ::uda]
          :opt-un [::dimension
                   ::generation
                   ::member-formula
                   ::parent
                   ::path
                   ::time-balance
                   ::variance-reporting
                   ::storage-category
                   ::misc]))

;; Memory savings and performance?
(defrecord OutlineMember [alias
                          aliases
                          attributes
                          consolidation
                          data-storage
                          level0?
                          members
                          name
                          shared-member
                          tag
                          contained
                          two-pass-calc
                          uda
                          dimension
                          generation
                          member-formula
                          parent
                          path
                          time-balance
                          variance-reporting
                          storage-category
                          hierarchy-type
                          misc])

(def member-keys (keys (map->OutlineMember nil)))